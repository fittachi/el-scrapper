#!/bin/bash
#https://stackoverflow.com/questions/47958163/tesseract-ocr-large-number-of-files
failed=()
while read -r line; do
  if ! timeout 2m node ./dist/scrapper.js --dont-use-proxy  -c config.json OCR "$line $3"; then
    echo "OCR timeout $line added to failed list"
    failed+=("$line")
  fi
done < <(find "$1" -name "$2")
echo "Failed Documents save in ocr.log"
for i in ${failed[*]}; do
  echo -e "$i" >>ocr.log
done
