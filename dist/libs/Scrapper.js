"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Scrapper = exports.ScrapperBuilder = exports.ScrapperStatus = void 0;
require('chromedriver');
require('geckodriver');
const chrome = __importStar(require("selenium-webdriver/chrome"));
const firefox = __importStar(require("selenium-webdriver/firefox"));
const selenium_webdriver_1 = require("selenium-webdriver");
const events_1 = require("events");
const http_1 = require("http");
const GeeTestPuzzleCaptchaSolver_1 = __importDefault(require("./GeeTestPuzzleCaptchaSolver"));
const Utils_1 = require("./Utils");
const con = Utils_1.logger('downloader');
var ScrapperStatus;
(function (ScrapperStatus) {
    ScrapperStatus[ScrapperStatus["ok"] = 0] = "ok";
    ScrapperStatus[ScrapperStatus["captchaLevel1"] = 1] = "captchaLevel1";
    ScrapperStatus[ScrapperStatus["captchaLevel2"] = 2] = "captchaLevel2";
    ScrapperStatus[ScrapperStatus["loadingError"] = 3] = "loadingError";
    ScrapperStatus[ScrapperStatus["unknown"] = 4] = "unknown";
})(ScrapperStatus = exports.ScrapperStatus || (exports.ScrapperStatus = {}));
class ScrapperBuilder {
    constructor() {
        this.browserName = 'firefox';
        this.baseServerPort = 3000;
    }
    static instance() {
        return new ScrapperBuilder();
    }
    browser(browserName) {
        this.browserName = browserName;
        return this;
    }
    downloadPath(downloadPath) {
        this.downloadPathAddress = downloadPath;
        return this;
    }
    internalServerPort(portNumber) {
        this.baseServerPort = portNumber;
        return this;
    }
    async build() {
        const scrapper = new Scrapper(this.downloadPathAddress, this.baseServerPort);
        await scrapper.initializeDriver(this.browserName);
        await scrapper.initializeServer();
        return scrapper;
    }
}
exports.ScrapperBuilder = ScrapperBuilder;
class Scrapper extends events_1.EventEmitter {
    constructor(downloadPath, baseServerPort) {
        super();
        this.logReferenceNumber = "0";
        this.timeout = false;
        this.failedTryCount = { count: 0, ref: "-1" };
        this.baseServerPort = baseServerPort;
        this.downloadPath = downloadPath;
    }
    createFireFox() {
        const browserOptions = new firefox.Options();
        browserOptions.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
        browserOptions.setPreference("browser.download.folderList", 2);
        browserOptions.setPreference("browser.download.manager.showWhenStarting", true);
        browserOptions.setPreference("browser.download.dir", this.downloadPath);
        browserOptions.setPreference("plugin.scan.plid.all", false);
        browserOptions.setPreference("plugin.scan.Acrobat", "99.0");
        browserOptions.setPreference("pdfjs.disabled", true);
        return new selenium_webdriver_1.Builder()
            .forBrowser('firefox')
            .setFirefoxOptions(browserOptions)
            .build();
    }
    createChrome() {
        return new selenium_webdriver_1.Builder()
            .forBrowser('chrome')
            .setChromeOptions(new chrome.Options().setUserPreferences({
            "download.prompt_for_download": false,
            "download.default_directory": this.downloadPath,
            "download.directory_upgrade": true,
            "plugins.always_open_pdf_externally": true,
            "download.extensions_to_open": "applications/pdf"
        }))
            .build();
    }
    needHelp(resolver) {
        if (resolver) {
            return this.emit('needHelp', resolver);
        }
        return new Promise(r => this.emit('needHelp', r));
    }
    captchaLevelOneHandler() {
        return new Promise(async (resolve) => {
            this.emit('setHelp', resolve);
            const solver = new GeeTestPuzzleCaptchaSolver_1.default(this.driver);
            setTimeout(async () => {
                let count = 0;
                let e;
                while (true) {
                    try {
                        count++;
                        if (count === 3) {
                            con.log('Error', e.message);
                            break;
                        }
                        const pos = await solver.activeTest();
                        await solver.audioSolver(pos);
                        break;
                    }
                    catch (err) {
                        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`);
                        await Utils_1.sleep(3000);
                        await this.driver.navigate().to(this.getQuery(this.logReferenceNumber));
                        e = err;
                    }
                }
            }, 2500);
            // this.emit('needHelp', resolve)
        });
    }
    setProxy(proxy) {
        this.proxy = proxy;
    }
    async captchaLevelTwoHandler(referenceNumber) {
        await this.driver.close();
        await this.proxy.changeIP();
        await this.initializeDriver('chrome');
        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`);
        await Utils_1.sleep(2000);
        await this.download(this.logReferenceNumber);
    }
    reloadPage(referenceNumber) {
        con.log('timeout or reload call for ', referenceNumber);
        return new Promise(async (resolve) => {
            await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`);
            setTimeout(async () => {
                await this.download(referenceNumber);
                resolve();
            }, 5000);
        });
    }
    async checkStatus() {
        await Utils_1.sleep(1000);
        let status = ScrapperStatus.unknown;
        try {
            await this.driver.findElement(selenium_webdriver_1.By.id('display-pages'));
            status = ScrapperStatus.captchaLevel1;
        }
        catch (e) {
        }
        if (status === ScrapperStatus.unknown) {
            try {
                await this.driver.findElement(selenium_webdriver_1.By.id('Free-5X-Search'));
                status = ScrapperStatus.captchaLevel2;
            }
            catch (e) {
            }
        }
        if (status === ScrapperStatus.unknown) {
            try {
                await this.driver.findElement(selenium_webdriver_1.By.id('reload-button'));
                status = ScrapperStatus.loadingError;
            }
            catch (e) {
            }
        }
        if (status === ScrapperStatus.unknown) {
            status = ScrapperStatus.ok;
        }
        this.emit('status', status);
        return status;
    }
    getQuery(referenceNumber) {
        return `https://www.equineline.com/Free-5X-Pedigree.cfm?source=RISA&page_state=GENERATE&nicking_stats_indicator=Y&reference_number=${referenceNumber}`;
    }
    initializeServer() {
        return new Promise(resolve => {
            this.baseServer = http_1.createServer((req, res) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/html');
                res.end(`<h1>Equineline Pedigree scrapper</h1> <p>downloaded ${this.logReferenceNumber}</p>`);
            }).listen(this.baseServerPort, resolve);
        });
    }
    async initializeDriver(browser) {
        if (browser === 'firefox') {
            this.driver = this.createFireFox();
        }
        else {
            this.driver = this.createChrome();
        }
    }
    downloadVerified() {
        if (this.timeout) {
            clearTimeout(this.timeout);
            con.log('download Verified', this.logReferenceNumber);
        }
        this.timeout = false;
    }
    async download(referenceNumber) {
        if (this.timeout !== false) {
            if (this.logReferenceNumber !== referenceNumber) {
                throw new Error('wrong download');
            }
        }
        else {
            this.timeout = setTimeout(() => {
                this.timeout = false;
                this.reloadPage(referenceNumber);
            }, 5 * 60 * 1000);
        }
        if (this.failedTryCount.ref === referenceNumber.toString()) {
            this.failedTryCount.count++;
            if (this.failedTryCount.count === 6) {
                this.failedTryCount.count = 0;
                con.log('failed 5 times', referenceNumber);
                con.log('reset downloader', referenceNumber);
                return this.captchaLevelTwoHandler(referenceNumber);
            }
        }
        else {
            this.failedTryCount.ref = referenceNumber.toString();
            this.failedTryCount.count = 0;
        }
        this.logReferenceNumber = referenceNumber.toString();
        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`);
        await Utils_1.sleep(100);
        await this.driver.navigate().to(this.getQuery(referenceNumber));
        const status = await this.checkStatus();
        con.log("status is", ScrapperStatus[status]);
        if (status === ScrapperStatus.captchaLevel1) {
            await this.captchaLevelOneHandler();
        }
        else if (status === ScrapperStatus.captchaLevel2) {
            await this.captchaLevelTwoHandler(referenceNumber);
        }
        else if (status === ScrapperStatus.loadingError) {
            await this.reloadPage(referenceNumber);
        }
        else if (status === ScrapperStatus.unknown) {
            throw new Error('unexpected situation (scrapper cant detect situation "captchaLevel1 or captchaLevel2 or ok")');
        }
        else {
            this.emit('downloadCompleted', referenceNumber);
        }
    }
    async close() {
        if (this.baseServer) {
            this.baseServer.close();
        }
        if (this.driver) {
            await this.driver.close();
        }
    }
}
exports.Scrapper = Scrapper;
