"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const selenium_webdriver_1 = require("selenium-webdriver");
const Utils_1 = require("./Utils");
const path_1 = require("path");
const child_process_1 = require("child_process");
const os_1 = require("os");
const fs_1 = __importDefault(require("fs"));
const Application_1 = require("./Application");
const numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
const con = Utils_1.logger('GeeTestPuzzleCaptchaSolver');
class GeeTestPuzzleCaptchaSolver {
    constructor(driver) {
        this.driver = driver;
    }
    async activeTest() {
        let end;
        while (true) {
            try {
                let { width, height } = await this.driver.manage().window().getRect();
                width -= 250;
                height -= 250;
                const initBTN = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className('geetest_radar_tip'));
                let offset = await initBTN.getRect();
                let action = this.driver.actions();
                let start = {
                    x: 0,
                    y: 0
                };
                end = {
                    x: 0,
                    y: 0
                };
                for (let i = 0; i < 3; i++) {
                    end = {
                        x: 50 + Math.floor(Math.random() * width),
                        y: 50 + Math.floor(Math.random() * height)
                    };
                    action = Utils_1.animationAction(action, start, end, Math.floor(Math.random() * 150) + 150);
                    action = action.pause(100);
                    start = end;
                }
                end = {
                    x: Math.floor(offset.x + 2),
                    y: Math.floor(offset.y + 3)
                };
                action = Utils_1.animationAction(action, start, end, Math.floor(Math.random() * 150) + 150).pause(150).click();
                await action.perform();
                break;
            }
            catch (e) {
            }
        }
        con.log('done part one');
        return end;
    }
    loadImages() {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                try {
                    const fullBg = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className("geetest_canvas_fullbg"));
                    const bg = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className("geetest_canvas_bg"));
                    await Utils_1.sleep(1500);
                    const fullBgBase64 = await this.driver.executeScript("return arguments[0].toDataURL('image/png').substring(21);", fullBg);
                    const bgBase64 = await this.driver.executeScript("return arguments[0].toDataURL('image/png').substring(21);", bg);
                    let diff = Utils_1.diffImage(Buffer.from(fullBgBase64, 'base64'), Buffer.from(bgBase64, 'base64'));
                    resolve(diff);
                }
                catch (e) {
                    reject(e);
                }
            }, 1000);
        });
    }
    async solve(diff, pos) {
        await Utils_1.sleep(1000);
        diff = diff.sort((a, b) => a.x - b.x);
        if (!diff[0]) {
            con.log('ops');
        }
        const value = diff[0].x + 7;
        await Utils_1.sleep(500);
        const sliderBtn = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className('geetest_slider_button'));
        let offset = await sliderBtn.getRect();
        const startPos = { x: Math.floor(offset.x) + 15, y: Math.floor(offset.y) + 15 };
        const endPos = {
            y: Math.floor(offset.y) + Math.floor(Math.random() * 20),
            x: Math.floor(offset.x) + Math.floor(value) + Math.floor(Math.random() * 2) * (Math.random() < 0.5 ? -1 : 1)
        };
        let action = this.driver.actions().move(pos).pause(50);
        action = Utils_1.animationAction(action, pos, startPos).pause(150);
        action = action.press();
        action = Utils_1.animationAction(action, startPos, endPos);
        await action.pause(50).release().perform();
        con.log('done part two');
    }
    convertAudio(inputPath, outPutPath) {
        return new Promise(((resolve, reject) => {
            setTimeout(async () => {
                const command = child_process_1.exec(`ffmpeg -i ${inputPath} -acodec pcm_s16le -ac 1 -ar 16000 ${outPutPath}`);
                command.on('error', reject);
                command.on('exit', code => {
                    if (code === 0)
                        resolve(outPutPath);
                    else
                        reject();
                });
            }, 500);
        }));
    }
    static ATT(inputPath) {
        return new Promise((resolve, reject) => {
            child_process_1.exec(`${Application_1.Application.config.deepSpeech.executable} --model ${Application_1.Application.config.deepSpeech.model} --scorer ${Application_1.Application.config.deepSpeech.score} --audio ${inputPath}`, (error, stdout, stderr) => {
                if (error)
                    reject(error);
                const code = stdout.split(os_1.EOL)[0].split(' ')
                    .map((r) => numbers.findIndex(d => d === r.toString().toLowerCase()).toString())
                    .filter(r => r !== '-1').join('');
                resolve(code);
            });
        });
    }
    async audioSolver(pos) {
        const audioBtn = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className('geetest_voice'));
        const offset = await audioBtn.getRect();
        let action = this.driver.actions().move(pos).pause(50);
        await Utils_1.animationAction(action, pos, {
            x: Math.floor(offset.x),
            y: Math.floor(offset.y)
        }).pause(50).click().perform();
        const audio = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className('geetest_music'), 20 * 1000);
        let audioUrl;
        await Utils_1.sleep(4000);
        while (!(typeof audioUrl === 'string' && audioUrl.toString().length > 0)) {
            await Utils_1.sleep(1000);
            audioUrl = await this.driver.executeScript("return arguments[0].currentSrc;", audio);
        }
        try {
            await fs_1.default.promises.unlink(path_1.join(__dirname, 'text.mp3'));
            await fs_1.default.promises.unlink(path_1.join(__dirname, 'text.wav'));
        }
        catch (e) {
            // ist ok
        }
        await Utils_1.downloadContent(path_1.join(__dirname, 'text.mp3'), audioUrl);
        con.log('original song downloaded at ', path_1.join(__dirname, 'text.mp3'));
        await this.convertAudio(path_1.join(__dirname, 'text.mp3'), path_1.join(__dirname, 'text.wav'));
        con.log('converted song save at ', path_1.join(__dirname, 'text.mp3'));
        const code = await GeeTestPuzzleCaptchaSolver.ATT(path_1.join(__dirname, 'text.wav'));
        con.log('code is ', code);
        const input = await Utils_1.findElByTimeOut(this.driver, selenium_webdriver_1.By.className('geetest_input'));
        await input.sendKeys(code);
        await Utils_1.sleep(150);
        await input.sendKeys(selenium_webdriver_1.Key.RETURN);
    }
}
exports.default = GeeTestPuzzleCaptchaSolver;
