"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = exports.downloadContent = exports.animationAction = exports.findPDF = exports.diffImage = exports.findElByTimeOut = exports.deleteFolderRecursive = exports.dropSimilar = exports.rand = exports.shuffle = exports.range = exports.sleep = exports.groupBy = void 0;
const fs = __importStar(require("fs"));
const Path = __importStar(require("path"));
const selenium_webdriver_1 = require("selenium-webdriver");
const pngjs_1 = require("pngjs");
const DatabaseManager_1 = require("./DatabaseManager");
const https_1 = require("https");
const con = logger('AnimationSimulator');
function groupBy(list, keyGetter) {
    const map = {};
    list.forEach((item) => {
        const key = keyGetter(item);
        if (!map.hasOwnProperty(key)) {
            map[key] = [];
        }
        map[key].push(item);
    });
    return map;
}
exports.groupBy = groupBy;
function sleep(timeout) {
    return new Promise(r => setTimeout(r, timeout));
}
exports.sleep = sleep;
function range(start, end) {
    return [...Array(end - start).keys()].map(r => r + start);
}
exports.range = range;
function shuffle(array) {
    return array.map(a => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map((a) => a[1]);
}
exports.shuffle = shuffle;
function rand(min, max) {
    if (Array.isArray(min)) {
        return Math.floor(Math.random() * (min[1] - min[0])) + min[0];
    }
    else {
        if (max) {
            return Math.floor(Math.random() * (max - min)) + min;
        }
        else
            throw new Error('not enough data');
    }
}
exports.rand = rand;
function dropSimilar(arr, threshold = 1) {
    return arr.filter((item, index, self) => Math.abs(self[index + 1] - item) > threshold || isNaN(self[index + 1]));
}
exports.dropSimilar = dropSimilar;
function deleteFolderRecursive(path) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach((file, index) => {
            const curPath = Path.join(path, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            }
            else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}
exports.deleteFolderRecursive = deleteFolderRecursive;
function findElByTimeOut(driver, by, timeout = 12500) {
    return driver.wait(selenium_webdriver_1.until.elementLocated(by), timeout);
}
exports.findElByTimeOut = findElByTimeOut;
function diffImage(imageBuf, imageBuf2) {
    const threshold = 5;
    const image1 = pngjs_1.PNG.sync.read(imageBuf);
    const image2 = pngjs_1.PNG.sync.read(imageBuf2);
    if (image1.width !== image2.width || image1.height !== image2.height) {
        throw new Error("images size not same");
    }
    const results = [];
    for (let y = 0; y < image1.height; y++) {
        for (let x = 0; x < image1.width; x++) {
            let idx = (image1.width * y + x) << 2;
            const pix1 = (image1.data[idx] + image1.data[idx + 1] + image1.data[idx + 2]) / 3 * (image1.data[idx + 3] / 255);
            const pix2 = (image2.data[idx] + image2.data[idx + 1] + image2.data[idx + 2]) / 3 * (image2.data[idx + 3] / 255);
            if (Math.abs(pix1 - pix2) >= threshold) {
                results.push({ x, y, diff: pix2 - pix1 });
            }
        }
    }
    return results;
}
exports.diffImage = diffImage;
function findPDF(refID) {
    return DatabaseManager_1.DownloadedPDF.findOne({
        where: {
            referenceNumber: refID
        }
    });
}
exports.findPDF = findPDF;
function animationAction(action, start, end, duration = 1000) {
    con.log(`go for ${JSON.stringify(start)} to ${JSON.stringify(end)}`);
    const threshold = 8;
    action = action.move(start);
    const it = duration / threshold;
    const deltaX = (end.x - start.x) / it;
    const deltaY = (end.y - start.y) / it;
    for (let i = 0; i < it; i++) {
        start.x += deltaX;
        start.y += deltaY;
        action = action
            .move({
            x: Math.floor(start.x),
            y: Math.floor(start.y),
        });
        // .pause(threshold)
    }
    action = action.move(end);
    return action;
}
exports.animationAction = animationAction;
function downloadContent(path, url) {
    return new Promise(resolve => {
        const file = fs.createWriteStream(path);
        file.once('finish', resolve);
        https_1.get(url, (response) => response.pipe(file));
    });
}
exports.downloadContent = downloadContent;
function logger(scopeName) {
    return {
        log(ref, message) {
            if (message)
                console.log(Date.now() / 1000, scopeName, ref, message);
            else
                console.log(Date.now() / 1000, scopeName, ref);
        }
    };
}
exports.logger = logger;
