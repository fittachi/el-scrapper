"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OCRManager = void 0;
const fs = __importStar(require("fs"));
const pngjs_1 = require("pngjs");
const path_1 = require("path");
const Utils_1 = require("./Utils");
const DatabaseManager_1 = require("./DatabaseManager");
const child_process_1 = require("child_process");
const progress_1 = __importDefault(require("progress"));
class OCRManager {
    constructor(OCRTempPath, forceToUpdate = false) {
        this.OCRTempPath = OCRTempPath;
        this.forceToUpdate = forceToUpdate;
    }
    convertPDFToImage(address, id) {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (process.platform === 'win32') {
                    const command = child_process_1.exec(`gm.exe convert  -density 300  -size 1275x1651 ${address} -resize 1275x1651  ${path_1.join(this.OCRTempPath, id + "-1.png")}`);
                    command.on('error', reject);
                    command.on('exit', code => {
                        const command2 = child_process_1.exec(`gm.exe convert  -density 300  -size 1275x1651 ${address}[1] -resize 1275x1651  ${path_1.join(this.OCRTempPath, id + "-2.png")}`);
                        command2.on('error', reject);
                        command2.on('exit', code => resolve(code === 0 ? path_1.join(this.OCRTempPath, id) : false));
                    });
                }
                else {
                    const command = child_process_1.exec(`pdftoppm ${address} ${path_1.join(this.OCRTempPath, id)} -png`);
                    command.on('error', reject);
                    command.on('exit', code => {
                        if (code === 0)
                            resolve(path_1.join(this.OCRTempPath, id));
                        else
                            reject();
                    });
                }
            }, 100);
        });
    }
    readImage(address, id, deleteTmpImage = true) {
        const progress = new progress_1.default('  OCR [:bar] :percent', {
            complete: '=',
            incomplete: ' ',
            width: 20,
            total: 72 + 43
        });
        let finalResult = {
            horse: new DatabaseManager_1.Horse,
            pedigree: new DatabaseManager_1.Pedigree,
            nickingStats: new DatabaseManager_1.NickingStats
        };
        let path;
        return new Promise(async (resolve, reject) => {
            let error;
            for (let i = 0; i < 5; i++) {
                try {
                    path = await this.convertPDFToImage(address, id);
                    console.log(path);
                    break;
                }
                catch (e) {
                    error = e;
                    await Utils_1.sleep(500);
                }
            }
            if (error)
                reject(error);
            progress.tick();
            if (typeof path === 'string') {
                const self = this;
                fs.createReadStream(path + "-1.png")
                    .pipe(new pngjs_1.PNG({
                    filterType: 4,
                }))
                    .on("parsed", async function () {
                    const { xArray, yArray } = await self.findLines(this);
                    progress.tick();
                    await self.cropImages(this, id, xArray, yArray, progress);
                    const result = await self.OCR(id, progress);
                    progress.tick();
                    finalResult.horse = result.horse;
                    finalResult.pedigree = result.pedigree;
                    fs.createReadStream(path + "-2.png")
                        .pipe(new pngjs_1.PNG({
                        filterType: 4,
                    }))
                        .on("parsed", async function () {
                        const yArray = [
                            245,
                            315,
                            390,
                        ];
                        const xArray = [
                            150,
                            300,
                            450,
                            600,
                            750,
                            900,
                            1050,
                            1200
                        ];
                        progress.tick(); // 1
                        const images = await self.cropImagesPage2(this, xArray, yArray, id, progress);
                        const result = await self.OCRPage2(id, images, progress);
                        finalResult.nickingStats = result;
                        resolve(finalResult);
                        if (deleteTmpImage) {
                            Utils_1.deleteFolderRecursive(path_1.join(self.OCRTempPath, id + "-tmp"));
                            Utils_1.deleteFolderRecursive(path_1.join(self.OCRTempPath, id + "-tmp-2"));
                            await fs.promises.unlink(path_1.join(self.OCRTempPath, id + "-1.png"));
                            await fs.promises.unlink(path_1.join(self.OCRTempPath, id + "-2.png"));
                        }
                    });
                });
            }
        });
    }
    async findLines(image) {
        const threshold = 120;
        const defaultXPadding = 36;
        let targets = [];
        let xMap = {};
        // extract y lines
        for (let y = 0; y < image.height; y++) {
            let blackPixelCount = 0;
            for (let x = 0; x < image.width; x++) {
                let idx = (image.width * y + x) << 2;
                const r = image.data[idx];
                const g = image.data[idx + 1];
                const b = image.data[idx + 2];
                const a = image.data[idx + 3];
                if (r === g && g === b && (b === a && a === b || b === 230 || b === 235)) {
                    image.data[idx + 3] = 0;
                }
                else if (r === g && g === b) {
                    if (r < threshold) {
                        image.data[idx] = 0;
                        image.data[idx + 1] = 0;
                        image.data[idx + 2] = 0;
                    }
                }
                if (!xMap.hasOwnProperty(x.toString())) {
                    xMap[x.toString()] = 0;
                }
                if (r === g && g === b && b === 0 && a !== 0) {
                    blackPixelCount++;
                    xMap[x.toString()] += 1;
                }
            }
            if (blackPixelCount >= image.width - 2 * defaultXPadding) {
                targets.push(y);
            }
            blackPixelCount = 0;
        }
        let xArray = [];
        for (let key in xMap) {
            xArray.push({ xIndex: Number(key), count: xMap[key] });
        }
        targets = Utils_1.dropSimilar(targets);
        const targetImageHeight = targets[targets.length - 1] - targets[0] - 3;
        const targetImageWidth = image.width - 2 * defaultXPadding - 6;
        xArray = xArray
            .filter(a => a.count >= targetImageHeight && (defaultXPadding + 3 < a.xIndex && a.xIndex < image.width - defaultXPadding - 3))
            .sort((a, b) => a.xIndex - b.xIndex)
            .filter((a, index) => index % 2 === 1)
            .map(r => r.xIndex);
        if (xArray.length < 5) {
            await fs.promises.writeFile(path_1.join(this.OCRTempPath, "debug.png"), pngjs_1.PNG.sync.write(image));
            throw new Error('cant find lines');
        }
        xArray.push(image.width - defaultXPadding - 2);
        xArray.unshift(defaultXPadding - 2);
        // extract x lines
        const yArray = [];
        for (let i = 0; i < 6; i++) {
            const boxCount = Math.pow(2, i);
            const lineCount = boxCount - 1;
            let miniTarget = [];
            for (let y = targets[0] + 2; y < targets[targets.length - 1] - 2; y++) {
                let blackPixelCount = 0;
                for (let x = xArray[i]; x < xArray[i + 1]; x++) {
                    let idx = (image.width * y + x) << 2;
                    const r = image.data[idx];
                    const g = image.data[idx + 1];
                    const b = image.data[idx + 2];
                    const a = image.data[idx + 3];
                    if (r === g && g === b && b === 0 && a !== 0) {
                        blackPixelCount++;
                    }
                }
                if (blackPixelCount >= xArray[i + 1] - xArray[i]) {
                    miniTarget.push(y);
                }
            }
            miniTarget = Utils_1.dropSimilar(miniTarget);
            yArray[i] = miniTarget;
            yArray[i].unshift(targets[0] + 2);
            yArray[i].push(targets[targets.length - 1] - 2);
        }
        return { xArray, yArray };
    }
    async cropImages(image, id, xArray, yArray, progress) {
        // crop image
        try {
            await fs.promises.mkdir(path_1.join(this.OCRTempPath, id + "-tmp"));
        }
        catch (e) {
            if (e.errno !== -17) {
                throw e;
            }
        }
        for (let i = 0; i < 6; i++) {
            const boxCount = Math.pow(2, i);
            for (let j = 0; j < boxCount; j++) {
                const pos = {
                    x: xArray[i],
                    y: yArray[i][j],
                    w: xArray[i + 1] - xArray[i],
                    h: yArray[i][j + 1] - yArray[i][j]
                };
                const dst = new pngjs_1.PNG({ width: pos.w, height: pos.h });
                image.bitblt(dst, pos.x, pos.y, pos.w, pos.h, 0, 0);
                await fs.promises.writeFile(path_1.join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png"), pngjs_1.PNG.sync.write(dst));
            }
            progress.tick();
        }
    }
    async cropImagesPage2(image, xArray, yArray, id, progress) {
        try {
            await fs.promises.mkdir(path_1.join(this.OCRTempPath, id + "-tmp-2"));
        }
        catch (e) {
            if (e.errno !== -17) {
                throw e;
            }
        }
        const results = [];
        for (let j = 0; j < yArray.length; j++) {
            results[j] = [];
            for (let i = 0; i < xArray.length - 1; i++) {
                const pos = {
                    x: xArray[i],
                    y: yArray[j],
                    w: xArray[i + 1] - xArray[i],
                    h: 30
                };
                const dst = new pngjs_1.PNG({ width: pos.w, height: pos.h });
                image.bitblt(dst, pos.x, pos.y, pos.w, pos.h, 0, 0);
                await fs.promises.writeFile(path_1.join(this.OCRTempPath, id + "-tmp-2", j + "-" + i + ".png"), pngjs_1.PNG.sync.write(dst));
                results[j].push(path_1.join(this.OCRTempPath, id + "-tmp-2", j + "-" + i + ".png"));
                progress.tick(); // 21
            }
        }
        return results;
    }
    async OCR(id, progress) {
        let p;
        let h;
        if (this.forceToUpdate) {
            let tp = await DatabaseManager_1.Pedigree.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            });
            let th = await DatabaseManager_1.Horse.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            });
            p = (!tp) ? new DatabaseManager_1.Pedigree() : tp;
            h = (!th) ? new DatabaseManager_1.Horse() : th;
        }
        else {
            p = new DatabaseManager_1.Pedigree();
            h = new DatabaseManager_1.Horse();
        }
        const horseText = await this.doOCR(path_1.join(this.OCRTempPath, id + "-tmp", 0 + "-" + 0 + ".png")).then(r => r.trim().split(/\r?\n|\r/g).filter(r => r.length > 0));
        h.referenceNumber = Number(id);
        h.name = horseText[0] || '';
        if (horseText[2].toLowerCase().startsWith('foaled')) {
            h.type = horseText[1] || '';
            h.foaledDate = horseText[2] || '';
            h.foaledIn = horseText[3] || '';
            h.startedGame = ((horseText[4] || '').endsWith('Starts')) ? Number(horseText[4].split(' ')[0]) : NaN;
            h.winner = ((horseText[4] || '').endsWith('Winner')) ? Number(horseText[4].split(' ')[0]) : NaN;
        }
        else {
            h.type = horseText[1] + " " + horseText[2] || '';
            h.foaledDate = horseText[3] || '';
            h.foaledIn = horseText[4] || '';
            h.startedGame = ((horseText[5] || '').endsWith('Starts')) ? Number(horseText[5].split(' ')[0]) : NaN;
            h.winner = ((horseText[5] || '').endsWith('Winner')) ? Number(horseText[5].split(' ')[0]) : NaN;
        }
        h.rawString = horseText[4] || '' + horseText[5] || '';
        progress.tick();
        const texts = [];
        for (let i = 1; i < 6; i++) {
            const boxCount = Math.pow(2, i);
            texts[i] = [];
            for (let j = 0; j < boxCount; j++) {
                texts[i][j] = await this.doOCR(path_1.join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png")).then(r => r.replace(/\r?\n|\r/g, '').trim());
                progress.tick();
            }
        }
        p.referenceNumber = h.referenceNumber;
        p.S = texts[1][0] || "";
        p.D = texts[1][1] || "";
        p.SS = texts[2][0] || "";
        p.SD = texts[2][1] || "";
        p.DS = texts[2][2] || "";
        p.DD = texts[2][3] || "";
        p.SSS = texts[3][0] || "";
        p.SSD = texts[3][1] || "";
        p.SDS = texts[3][2] || "";
        p.SDD = texts[3][3] || "";
        p.DSS = texts[3][4] || "";
        p.DSD = texts[3][5] || "";
        p.DDS = texts[3][6] || "";
        p.DDD = texts[3][7] || "";
        p.SSSS = texts[4][0] || "";
        p.SSSD = texts[4][1] || "";
        p.SSDS = texts[4][2] || "";
        p.SSDD = texts[4][3] || "";
        p.SDSS = texts[4][4] || "";
        p.SDSD = texts[4][5] || "";
        p.SDDS = texts[4][6] || "";
        p.SDDD = texts[4][7] || "";
        p.DSSS = texts[4][8] || "";
        p.DSSD = texts[4][9] || "";
        p.DSDS = texts[4][10] || "";
        p.DSDD = texts[4][11] || "";
        p.DDSS = texts[4][12] || "";
        p.DDSD = texts[4][13] || "";
        p.DDDS = texts[4][14] || "";
        p.DDDD = texts[4][15] || "";
        p.SSSSS = texts[5][0] || "";
        p.SSSSD = texts[5][1] || "";
        p.SSSDS = texts[5][2] || "";
        p.SSSDD = texts[5][3] || "";
        p.SSDSS = texts[5][4] || "";
        p.SSDSD = texts[5][5] || "";
        p.SSDDS = texts[5][6] || "";
        p.SSDDD = texts[5][7] || "";
        p.SDSSS = texts[5][8] || "";
        p.SDSSD = texts[5][9] || "";
        p.SDSDS = texts[5][10] || "";
        p.SDSDD = texts[5][11] || "";
        p.SDDSS = texts[5][12] || "";
        p.SDDSD = texts[5][13] || "";
        p.SDDDS = texts[5][14] || "";
        p.SDDDD = texts[5][15] || "";
        p.DSSSS = texts[5][16] || "";
        p.DSSSD = texts[5][17] || "";
        p.DSSDS = texts[5][18] || "";
        p.DSSDD = texts[5][19] || "";
        p.DSDSS = texts[5][20] || "";
        p.DSDSD = texts[5][21] || "";
        p.DSDDS = texts[5][22] || "";
        p.DSDDD = texts[5][23] || "";
        p.DDSSS = texts[5][24] || "";
        p.DDSSD = texts[5][25] || "";
        p.DDSDS = texts[5][26] || "";
        p.DDSDD = texts[5][27] || "";
        p.DDDSS = texts[5][28] || "";
        p.DDDSD = texts[5][29] || "";
        p.DDDDS = texts[5][30] || "";
        p.DDDDD = texts[5][31] || "";
        return { horse: h, pedigree: p };
    }
    async OCRPage2(id, images, progress) {
        let t;
        if (this.forceToUpdate) {
            let tmp = await DatabaseManager_1.NickingStats.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            });
            t = (!tmp) ? new DatabaseManager_1.NickingStats() : tmp;
        }
        else {
            t = new DatabaseManager_1.NickingStats();
        }
        const texts = [];
        for (let j = 0; j < 3; j++) {
            texts[j] = [];
            for (let i = 0; i < 7; i++) {
                if (images[j][i]) {
                    texts[j][i] = await this.doOCR(images[j][i]).then(s => s.trim());
                    progress.tick();
                }
                else {
                    console.log(j, i);
                }
            }
        }
        t.referenceNumber = Number(id);
        t.s_foals = texts[0][1] || '';
        t.s_starters = texts[0][2] || '';
        t.s_winners = texts[0][3] || '';
        t.s_bw = texts[0][4] || '';
        t.s_earnings = texts[0][5] || '';
        t.s_aei = texts[0][6] || '';
        t.bs_mares = texts[1][0] || '';
        t.bs_foals = texts[1][1] || '';
        t.bs_starters = texts[1][2] || '';
        t.bs_winners = texts[1][3] || '';
        t.bs_bw = texts[1][4] || '';
        t.bs_earnings = texts[1][5] || '';
        t.bs_aei = texts[1][6] || '';
        t.bs_s_mares = texts[2][0] || '';
        t.bs_s_foals = texts[2][1] || '';
        t.bs_s_starters = texts[2][2] || '';
        t.bs_s_winners = texts[2][3] || '';
        t.bs_s_bw = texts[2][4] || '';
        t.bs_s_earnings = texts[2][5] || '';
        t.bs_s_aei = texts[2][6] || '';
        return t;
    }
    PPOCR(id, progress) {
        return new Promise(async (resolve) => {
            let p;
            let h;
            if (this.forceToUpdate) {
                let tp = await DatabaseManager_1.Pedigree.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                });
                let th = await DatabaseManager_1.Horse.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                });
                p = (!tp) ? new DatabaseManager_1.Pedigree() : tp;
                h = (!th) ? new DatabaseManager_1.Horse() : th;
            }
            else {
                p = new DatabaseManager_1.Pedigree();
                h = new DatabaseManager_1.Horse();
            }
            const horseText = await this.doOCR(path_1.join(this.OCRTempPath, id + "-tmp", 0 + "-" + 0 + ".png")).then(r => r.trim().split(/\r?\n|\r/g).filter(r => r.length > 0));
            h.referenceNumber = Number(id);
            h.name = horseText[0] || '';
            if (horseText[2].toLowerCase().startsWith('foaled')) {
                h.type = horseText[1] || '';
                h.foaledDate = horseText[2] || '';
                h.foaledIn = horseText[3] || '';
                h.startedGame = ((horseText[4] || '').endsWith('Starts')) ? Number(horseText[4].split(' ')[0]) : NaN;
                h.winner = ((horseText[4] || '').endsWith('Winner')) ? Number(horseText[4].split(' ')[0]) : NaN;
            }
            else {
                h.type = horseText[1] + " " + horseText[2] || '';
                h.foaledDate = horseText[3] || '';
                h.foaledIn = horseText[4] || '';
                h.startedGame = ((horseText[5] || '').endsWith('Starts')) ? Number(horseText[5].split(' ')[0]) : NaN;
                h.winner = ((horseText[5] || '').endsWith('Winner')) ? Number(horseText[5].split(' ')[0]) : NaN;
            }
            h.rawString = horseText[4] || '' + horseText[5] || '';
            progress.tick();
            const texts = [];
            let done = 0;
            const max = 62;
            function cb(text, i, j) {
                console.log('1', i, j, 'done');
                texts[i][j] = text;
                progress.tick();
                done++;
                if (done === max) {
                    p.referenceNumber = h.referenceNumber;
                    p.S = texts[1][0] || "";
                    p.D = texts[1][1] || "";
                    p.SS = texts[2][0] || "";
                    p.SD = texts[2][1] || "";
                    p.DS = texts[2][2] || "";
                    p.DD = texts[2][3] || "";
                    p.SSS = texts[3][0] || "";
                    p.SSD = texts[3][1] || "";
                    p.SDS = texts[3][2] || "";
                    p.SDD = texts[3][3] || "";
                    p.DSS = texts[3][4] || "";
                    p.DSD = texts[3][5] || "";
                    p.DDS = texts[3][6] || "";
                    p.DDD = texts[3][7] || "";
                    p.SSSS = texts[4][0] || "";
                    p.SSSD = texts[4][1] || "";
                    p.SSDS = texts[4][2] || "";
                    p.SSDD = texts[4][3] || "";
                    p.SDSS = texts[4][4] || "";
                    p.SDSD = texts[4][5] || "";
                    p.SDDS = texts[4][6] || "";
                    p.SDDD = texts[4][7] || "";
                    p.DSSS = texts[4][8] || "";
                    p.DSSD = texts[4][9] || "";
                    p.DSDS = texts[4][10] || "";
                    p.DSDD = texts[4][11] || "";
                    p.DDSS = texts[4][12] || "";
                    p.DDSD = texts[4][13] || "";
                    p.DDDS = texts[4][14] || "";
                    p.DDDD = texts[4][15] || "";
                    p.SSSSS = texts[5][0] || "";
                    p.SSSSD = texts[5][1] || "";
                    p.SSSDS = texts[5][2] || "";
                    p.SSSDD = texts[5][3] || "";
                    p.SSDSS = texts[5][4] || "";
                    p.SSDSD = texts[5][5] || "";
                    p.SSDDS = texts[5][6] || "";
                    p.SSDDD = texts[5][7] || "";
                    p.SDSSS = texts[5][8] || "";
                    p.SDSSD = texts[5][9] || "";
                    p.SDSDS = texts[5][10] || "";
                    p.SDSDD = texts[5][11] || "";
                    p.SDDSS = texts[5][12] || "";
                    p.SDDSD = texts[5][13] || "";
                    p.SDDDS = texts[5][14] || "";
                    p.SDDDD = texts[5][15] || "";
                    p.DSSSS = texts[5][16] || "";
                    p.DSSSD = texts[5][17] || "";
                    p.DSSDS = texts[5][18] || "";
                    p.DSSDD = texts[5][19] || "";
                    p.DSDSS = texts[5][20] || "";
                    p.DSDSD = texts[5][21] || "";
                    p.DSDDS = texts[5][22] || "";
                    p.DSDDD = texts[5][23] || "";
                    p.DDSSS = texts[5][24] || "";
                    p.DDSSD = texts[5][25] || "";
                    p.DDSDS = texts[5][26] || "";
                    p.DDSDD = texts[5][27] || "";
                    p.DDDSS = texts[5][28] || "";
                    p.DDDSD = texts[5][29] || "";
                    p.DDDDS = texts[5][30] || "";
                    p.DDDDD = texts[5][31] || "";
                    resolve({ horse: h, pedigree: p });
                }
            }
            for (let i = 1; i < 6; i++) {
                const boxCount = Math.pow(2, i);
                texts[i] = [];
                for (let j = 0; j < boxCount; j++) {
                    console.log('1', i, j, 'start');
                    this.doOCR(path_1.join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png"))
                        .then(r => r.replace(/\r?\n|\r/g, '').trim())
                        .then(r => cb(r, i, j));
                }
            }
        });
    }
    async PPOCRPage2(id, images, progress) {
        return new Promise(async (resolve) => {
            let t;
            if (this.forceToUpdate) {
                let tmp = await DatabaseManager_1.NickingStats.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                });
                t = (!tmp) ? new DatabaseManager_1.NickingStats() : tmp;
            }
            else {
                t = new DatabaseManager_1.NickingStats();
            }
            const texts = [];
            const max = 21;
            let done = 0;
            function cb(text, i, j) {
                console.log('2', i, j, 'done');
                texts[j][i] = text;
                progress.tick();
                done++;
                if (done === max) {
                    t.referenceNumber = Number(id);
                    t.s_foals = texts[0][1] || '';
                    t.s_starters = texts[0][2] || '';
                    t.s_winners = texts[0][3] || '';
                    t.s_bw = texts[0][4] || '';
                    t.s_earnings = texts[0][5] || '';
                    t.s_aei = texts[0][6] || '';
                    t.bs_mares = texts[1][0] || '';
                    t.bs_foals = texts[1][1] || '';
                    t.bs_starters = texts[1][2] || '';
                    t.bs_winners = texts[1][3] || '';
                    t.bs_bw = texts[1][4] || '';
                    t.bs_earnings = texts[1][5] || '';
                    t.bs_aei = texts[1][6] || '';
                    t.bs_s_mares = texts[2][0] || '';
                    t.bs_s_foals = texts[2][1] || '';
                    t.bs_s_starters = texts[2][2] || '';
                    t.bs_s_winners = texts[2][3] || '';
                    t.bs_s_bw = texts[2][4] || '';
                    t.bs_s_earnings = texts[2][5] || '';
                    t.bs_s_aei = texts[2][6] || '';
                    resolve(t);
                }
            }
            for (let j = 0; j < 3; j++) {
                texts[j] = [];
                for (let i = 0; i < 7; i++) {
                    if (images[j][i]) {
                        console.log('2', i, j, 'start');
                        this.doOCR(images[j][i]).then(s => s.trim()).then(r => cb(r, i, j));
                    }
                    else {
                        console.log(j, i);
                    }
                }
            }
        });
    }
    doOCR(address) {
        return new Promise(async (resolve, reject) => {
            if (process.platform === 'win32') {
                const command = child_process_1.exec(`tesseract.exe ${address} stdout`, (error, stdout) => {
                    if (error)
                        reject(error);
                    else {
                        resolve(stdout);
                    }
                });
            }
            else {
                const command = child_process_1.exec(`tesseract ${address} stdout`, (error, stdout) => {
                    if (error)
                        reject(error);
                    else {
                        resolve(stdout);
                    }
                });
            }
        });
    }
}
exports.OCRManager = OCRManager;
