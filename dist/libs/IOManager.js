"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IOManager = void 0;
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const events_1 = require("events");
let IOManager = /** @class */ (() => {
    class IOManager extends events_1.EventEmitter {
        constructor(tmpPath) {
            super();
            this.batch = [];
            this.isBatchChecked = false;
            const tmp = path_1.default.resolve(path_1.default.join(__dirname, '..', '..'), path_1.default.normalize(tmpPath));
            fs_1.default.accessSync(tmp, fs_1.default.constants.R_OK | fs_1.default.constants.W_OK);
            this.path = tmp;
            this.browserPath = path_1.default.join(this.path, IOManager.BrowserTmpPrefix);
            this.PDFsPath = path_1.default.join(this.path, IOManager.PDFsTmpPrefix);
            this.OCRPath = path_1.default.join(this.path, IOManager.OCRTmpPrefix);
        }
        async fixPath(pathAddress) {
            if (fs_1.default.existsSync(pathAddress)) {
                if (await fs_1.default.promises.stat(pathAddress).then(r => !r.isDirectory())) {
                    throw new Error(`${pathAddress} in not Directory`);
                }
            }
            else {
                await fs_1.default.promises.mkdir(pathAddress);
            }
        }
        async addBatch(id) {
            if (!fs_1.default.existsSync(path_1.default.join(this.PDFsPath, id))) {
                try {
                    await fs_1.default.promises.mkdir(path_1.default.join(this.PDFsPath, id));
                }
                catch (e) {
                    console.log(e);
                    process.exit(1);
                }
            }
            this.batch.unshift(id);
        }
        async updateBatchs() {
            this.batch = await fs_1.default.promises.readdir(this.PDFsPath, { withFileTypes: true })
                .then(dir => dir.filter(d => d.isDirectory()).map(d => d.name).sort().reverse());
            if (this.batch.length === 0) {
                await this.addBatch("1");
            }
        }
        get browserDownloadPath() {
            return this.browserPath;
        }
        get OCRTempPath() {
            return this.OCRPath;
        }
        get PDFsTempPath() {
            return this.PDFsPath;
        }
        async checkBatch(id) {
            if (!id)
                return true;
            return fs_1.default.promises.readdir(path_1.default.join(this.PDFsPath, id)).then(result => result.length < IOManager.batchSize);
        }
        async getBatch() {
            if (!await this.checkBatch(this.batch[0])) {
                this.addBatch((Number(this.batch[0]) + 1).toString());
            }
            return this.batch[0];
        }
        browserPathChangeHandler(eventType, filename) {
            if (eventType === 'rename' && filename === IOManager.defaultPDFName) {
                if (fs_1.default.existsSync(path_1.default.join(this.browserPath, IOManager.defaultPDFName))) {
                    this.emit('resolve');
                }
            }
        }
        async prepareMainPath() {
            await this.fixPath(this.browserPath);
            await this.fixPath(this.PDFsPath);
            await this.fixPath(this.OCRPath);
            await this.fixDownloadPath();
            await this.updateBatchs();
        }
        async fixDownloadPath() {
            if (fs_1.default.existsSync(path_1.default.join(this.browserPath, IOManager.defaultPDFName))) {
                await fs_1.default.promises.unlink(path_1.default.join(this.browserPath, IOManager.defaultPDFName));
            }
        }
        registerBrowserWatcher() {
            this.browserPathWatcher = fs_1.default.watch(this.browserPath, this.browserPathChangeHandler.bind(this));
            return this;
        }
        prepareFile(id) {
            return new Promise(async (resolve) => {
                const batchId = await this.getBatch();
                const finalAddress = path_1.default.join(this.PDFsPath, batchId, `${id}.pdf`);
                setTimeout(async () => {
                    try {
                        await fs_1.default.promises.rename(path_1.default.join(this.browserPath, IOManager.defaultPDFName), finalAddress);
                    }
                    catch (e) {
                        if (e.errno !== -2) {
                            console.log(e);
                            process.exit(1);
                        }
                    }
                    this.emit('pdf:ready', { batchId, id });
                    resolve({ batchId, id });
                }, 5);
            });
        }
        async close() {
            if (this.browserPathWatcher) {
                this.browserPathWatcher.close();
            }
            const data = await fs_1.default.promises.readdir(this.browserDownloadPath);
            for (const file in data) {
                if (file !== '.' && file !== '..') {
                    await fs_1.default.promises.unlink(file);
                }
            }
        }
    }
    IOManager.BrowserTmpPrefix = 'browser';
    IOManager.PDFsTmpPrefix = 'PDFs';
    IOManager.OCRTmpPrefix = 'OCRTmp';
    IOManager.defaultPDFName = 'Free-5X-Pedigree.pdf';
    IOManager.batchSize = 100;
    return IOManager;
})();
exports.IOManager = IOManager;
