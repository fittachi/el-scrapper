"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NickingStats = exports.DownloadedPDF = exports.Pedigree = exports.Horse = void 0;
const sequelize_1 = require("sequelize");
let Horse = /** @class */ (() => {
    class Horse extends sequelize_1.Model {
    }
    // public breeder!: string
    // public inbreeding!: string
    Horse.attributesType = {
        referenceNumber: {
            type: sequelize_1.DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: sequelize_1.DataTypes.STRING
        },
        foaledDate: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        foaledIn: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        type: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        startedGame: {
            type: sequelize_1.DataTypes.INTEGER,
            allowNull: true
        },
        winner: {
            type: sequelize_1.DataTypes.INTEGER,
            allowNull: true
        },
        rawString: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        }
        // breeder: {
        //     type: DataTypes.STRING,
        //     allowNull: true
        // },
        // inbreeding: {
        //     type: DataTypes.STRING,
        //     allowNull: true
        // }
    };
    return Horse;
})();
exports.Horse = Horse;
let Pedigree = /** @class */ (() => {
    class Pedigree extends sequelize_1.Model {
    }
    Pedigree.attributesType = {
        referenceNumber: {
            type: sequelize_1.DataTypes.INTEGER,
            primaryKey: true
        },
        S: {
            type: sequelize_1.DataTypes.STRING
        },
        D: {
            type: sequelize_1.DataTypes.STRING
        },
        SS: {
            type: sequelize_1.DataTypes.STRING
        },
        SD: {
            type: sequelize_1.DataTypes.STRING
        },
        DS: {
            type: sequelize_1.DataTypes.STRING
        },
        DD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SSDDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        SDDDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DSDDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDSDD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDSS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDSD: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDDS: {
            type: sequelize_1.DataTypes.STRING
        },
        DDDDD: {
            type: sequelize_1.DataTypes.STRING
        },
    };
    return Pedigree;
})();
exports.Pedigree = Pedigree;
let DownloadedPDF = /** @class */ (() => {
    class DownloadedPDF extends sequelize_1.Model {
    }
    DownloadedPDF.attributesType = {
        referenceNumber: {
            type: sequelize_1.DataTypes.INTEGER,
            primaryKey: true
        },
        hostname: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        user: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        path: {
            type: sequelize_1.DataTypes.STRING
        }
    };
    return DownloadedPDF;
})();
exports.DownloadedPDF = DownloadedPDF;
let NickingStats = /** @class */ (() => {
    class NickingStats extends sequelize_1.Model {
    }
    NickingStats.attributesType = {
        referenceNumber: {
            type: sequelize_1.DataTypes.INTEGER,
            primaryKey: true
        },
        s_foals: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        s_starters: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        s_winners: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        s_bw: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        s_earnings: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        s_aei: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_mares: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_foals: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_starters: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_winners: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_bw: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_earnings: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_aei: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_mares: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_foals: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_starters: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_winners: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_bw: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_earnings: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
        bs_s_aei: {
            type: sequelize_1.DataTypes.STRING,
            allowNull: true
        },
    };
    return NickingStats;
})();
exports.NickingStats = NickingStats;
class DatabaseManager {
    constructor(connectionQuery) {
        this.connection = new sequelize_1.Sequelize(connectionQuery, { logging: false });
        Horse.init(Horse.attributesType, {
            sequelize: this.connection,
            timestamps: true
        });
        Horse.sync();
        Pedigree.init(Pedigree.attributesType, {
            sequelize: this.connection,
            timestamps: true
        });
        Pedigree.sync();
        DownloadedPDF.init(DownloadedPDF.attributesType, {
            sequelize: this.connection,
            timestamps: true
        });
        DownloadedPDF.sync();
        NickingStats.init(NickingStats.attributesType, {
            sequelize: this.connection,
            timestamps: true
        });
        NickingStats.sync();
    }
    connect() {
        return this.connection.authenticate();
    }
    disconnect() {
        return this.connection.close();
    }
    async close() {
        this.disconnect();
    }
}
exports.default = DatabaseManager;
