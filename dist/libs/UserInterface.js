"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserInterface = void 0;
const node_notifier_1 = require("node-notifier");
const commander_1 = require("commander");
const fs_1 = require("fs");
const events_1 = require("events");
class UserInterface extends events_1.EventEmitter {
    constructor() {
        super();
        this.argsManager = new commander_1.Command();
        this.argsManager
            .version("version 0.8.0", "-v, --version", "show program version")
            .description("Equineline Pedigree scrapper")
            .option("-c, --config-file <pathToConfigFile>", "config file for program")
            .option("--dont-use-proxy", "dont change IP")
            .option("--proxy <proxy>", "change vpn provider", "both")
            .option('--force', 'force to update');
        this.argsManager
            .command('save <referenceNumber>')
            .description("download OCR and save an equineline document")
            .action((referenceNumber) => {
            setTimeout(() => this.emit('save', referenceNumber));
        });
        this.argsManager
            .command('download <referenceNumber>')
            .description("download an equineline document")
            .action((referenceNumber) => {
            setTimeout(() => this.emit('download', referenceNumber));
        });
        this.argsManager
            .command('OCR <fileAddress>')
            .description("OCR an equineline document")
            .action((fileAddress) => {
            setTimeout(() => this.emit('ocr', fileAddress));
        });
        // bulk
        this.argsManager
            .command('download-bulk <referenceNumberRange>')
            .description("download equineline documents EX: 150-2000")
            .action((referenceNumberRange) => {
            setTimeout(() => this.emit('download:bulk', referenceNumberRange));
        });
        this.argsManager
            .command('save-bulk <referenceNumberRange>')
            .description("download OCR and save equineline documents EX: 150-2000")
            .action((referenceNumberRange) => {
            setTimeout(() => this.emit('save:bulk', referenceNumberRange));
        });
        this.argsManager
            .command('register-pdfs <pattern>')
            .description('register pdfs to database as downloaded ex: ./tmp/PDFs/**/*.pdf')
            .action((pattern) => {
            setTimeout(() => this.emit('register:pdfs', pattern));
        });
        this.argsManager
            .command('category-pdfs <pattern> <destination>')
            .description('move pdfs to destination as 1000 chunks ex: "./tmp/PDFs/**/*.pdf" "./dist"')
            .action((pattern, destination) => {
            setTimeout(() => this.emit('category:pdfs', { pattern, destination }));
        });
    }
    notify(config) {
        node_notifier_1.notify(config);
    }
    readConfigFile(path) {
        if (!fs_1.existsSync(path)) {
            throw new Error(`Config file not found config path: "${path}"`);
        }
        const tmp = JSON.parse(fs_1.readFileSync(path).toString());
        this.programConfig = tmp;
        this.emit('config:ready', this.programConfig);
    }
    parseArrangement() {
        this.argsManager.parse(process.argv);
        const configPath = this.argsManager.configFile ? this.argsManager.configFile : "config.json";
        this.readConfigFile(configPath);
        return this;
    }
    get config() {
        return this.programConfig;
    }
    static build() {
        return new UserInterface();
    }
    get dontUseProxy() {
        return this.argsManager.dontUseProxy;
    }
    get proxy() {
        return this.argsManager.proxy;
    }
    get force() {
        return this.argsManager.force;
    }
}
exports.UserInterface = UserInterface;
