"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Application = void 0;
const UserInterface_1 = require("./UserInterface");
const Scrapper_1 = require("./Scrapper");
const IOManager_1 = require("./IOManager");
const path_1 = require("path");
const OCRManager_1 = require("./OCRManager");
const Utils_1 = require("./Utils");
const DatabaseManager_1 = __importStar(require("./DatabaseManager"));
const IPChanger_1 = require("./IPChanger");
const os_1 = __importDefault(require("os"));
const glob_1 = __importDefault(require("glob"));
const lodash_chunk_1 = __importDefault(require("lodash.chunk"));
const fs_1 = require("fs");
const con = Utils_1.logger('Application');
let Application = /** @class */ (() => {
    class Application {
        constructor() {
            this.helpFunction = false;
            this.selectedId = '';
            this.count = 0;
            this.ids = [];
            this.lastResolve = 0;
            this.closed = false;
            this.userInterface = UserInterface_1.UserInterface.build()
                .on('config:ready', async (config) => {
                Application.config = config;
            })
                .on('download', async (ref) => {
                this.ids = [Number(ref)];
                this.selectedId = ref.toString();
                await this.init().catch(error => {
                    throw error;
                });
                await this.initBrowser().catch(error => {
                    throw error;
                });
                this.io.once('resolve', async () => {
                    this.callHelpResolver();
                    const resultInfo = await this.io.prepareFile(ref);
                    await this.logDownload(resultInfo).catch(error => {
                        throw error;
                    });
                    await this.close().catch(error => {
                        throw error;
                    });
                });
                setTimeout(this.downloadNext.bind(this, false));
            })
                .on('ocr', async (fileAddress) => {
                await this.init().catch(error => {
                    throw error;
                });
                const path = path_1.parse(fileAddress);
                if (!this.userInterface.force) {
                    let tp = await DatabaseManager_1.NickingStats.findOne({
                        where: {
                            referenceNumber: Number(path.name)
                        }
                    });
                    if (tp) {
                        con.log(`it seems that this file (#${path.name}) has been added to database before`);
                        await this.close();
                        process.exit(1);
                    }
                }
                const result = await this.ocr.readImage(fileAddress, path.name, false).catch(error => {
                    throw error;
                });
                try {
                    await result.pedigree.save();
                    await result.horse.save();
                    await result.nickingStats.save();
                }
                catch (e) {
                    con.log(e.parent.message);
                    con.log(e.message);
                }
                finally {
                    await this.close().catch(error => {
                        throw error;
                    });
                }
                con.log('data saved to database', fileAddress);
            })
                .on('save', async (ref) => {
                this.ids = [Number(ref)];
                this.selectedId = ref.toString();
                await this.init().catch(error => {
                    throw error;
                });
                await this.initBrowser().catch(error => {
                    throw error;
                });
                this.io.once('resolve', async () => {
                    this.callHelpResolver();
                    const resultInfo = await this.io.prepareFile(ref);
                    await this.logDownload(resultInfo).catch(error => {
                        throw error;
                    });
                    await this.doOcr(resultInfo).catch(error => {
                        throw error;
                    });
                    await this.close().catch(error => {
                        throw error;
                    });
                });
                setTimeout(this.downloadNext.bind(this, false));
            })
                .on('save:bulk', async (referenceNumberRange) => {
                await this.init().catch(error => {
                    throw error;
                });
                this.parseRangeInput(referenceNumberRange);
                await this.initBrowser().catch(error => {
                    throw error;
                });
                this.io.on('resolve', async () => {
                    if (!await this.checkResolve())
                        return;
                    const resultInfo = await this.io.prepareFile(this.selectedId);
                    await this.logDownload(resultInfo).catch(error => {
                        throw error;
                    });
                    await this.doOcr(resultInfo).catch(error => {
                        throw error;
                    });
                    this.count++;
                    this.selectedId = (this.ids[this.count] || '').toString();
                    await this.downloadNext().catch(error => {
                        throw error;
                    });
                });
                setTimeout(this.downloadNext.bind(this, false));
            })
                .on('download:bulk', async (referenceNumberRange) => {
                await this.init().catch(error => {
                    throw error;
                });
                this.parseRangeInput(referenceNumberRange);
                await this.initBrowser().catch(error => {
                    throw error;
                });
                this.io.on('resolve', async () => {
                    if (!await this.checkResolve())
                        return;
                    const resultInfo = await this.io.prepareFile(this.selectedId);
                    await this.logDownload(resultInfo).catch(error => {
                        throw error;
                    });
                    this.count++;
                    this.selectedId = (this.ids[this.count] || '').toString();
                    await this.downloadNext().catch(error => {
                        throw error;
                    });
                });
                setTimeout(this.downloadNext.bind(this, false));
            })
                .on('register:pdfs', async (pattern) => {
                await this.init();
                glob_1.default(pattern, async (er, files) => {
                    if (er)
                        throw er;
                    for (let item of files) {
                        const id = Number(path_1.parse(item).name);
                        let info = await DatabaseManager_1.DownloadedPDF.findOne({
                            where: {
                                referenceNumber: Number(id)
                            }
                        });
                        if (!info) {
                            info = new DatabaseManager_1.DownloadedPDF();
                        }
                        info.hostname = os_1.default.hostname();
                        info.path = path_1.join(process.cwd(), item);
                        info.referenceNumber = id;
                        info.user = os_1.default.userInfo().username;
                        await info.save();
                        con.log(`#${path_1.parse(item).name} added to database`);
                    }
                    await this.close().catch(error => {
                        throw error;
                    });
                });
            })
                .on('category:pdfs', async ({ pattern, destination }) => {
                glob_1.default(pattern, async (er, files) => {
                    if (er)
                        throw er;
                    files = files
                        .filter(r => !!path_1.extname(r))
                        .sort((a, b) => a > b ? -1 : 1);
                    const chunks = lodash_chunk_1.default(files, 1000);
                    chunks.forEach(async (chunk, index) => {
                        console.log('start', index);
                        const path = path_1.join(destination, index.toString());
                        await fs_1.promises.mkdir(path);
                        for (let file of chunk) {
                            const info = path_1.parse(file);
                            await fs_1.promises.rename(file, path_1.join(path, info.base));
                        }
                        console.log('end', index);
                    });
                });
            })
                .parseArrangement();
        }
        async init() {
            this.io = new IOManager_1.IOManager(Application.config.downloadPath);
            await this.io.prepareMainPath().catch(error => {
                throw error;
            });
            this.io.registerBrowserWatcher();
            this.db = new DatabaseManager_1.default(Application.config.databaseConnectionQuery);
            this.ocr = new OCRManager_1.OCRManager(this.io.OCRTempPath, this.userInterface.force);
            this.proxy = new IPChanger_1.IPChanger(this.userInterface.dontUseProxy, this.userInterface.proxy);
            await this.proxy.loadServersData().catch(error => {
                throw error;
            });
            this.downloader = new Scrapper_1.Scrapper(this.io.browserDownloadPath, Application.config.port);
            this.downloader.setProxy(this.proxy);
            this.downloader
                .on("needHelp", (r) => {
                this.helpFunction = r;
                this.userInterface.notify({
                    title: "Scrapper need help",
                    message: "Please solve the puzzle."
                });
                con.log("scrapper need help");
            })
                .on('setHelp', r => this.helpFunction = r);
            process.once('SIGINT', async () => {
                con.log("Caught interrupt signal");
                await this.close();
                process.exit(1);
            });
        }
        async initBrowser() {
            try {
                while (!await this.proxy.changeIP()) {
                }
            }
            catch (e) {
                con.log(e.message);
            }
            await this.downloader.initializeServer().catch(error => {
                throw error;
            });
            await this.downloader.initializeDriver("chrome").catch(error => {
                throw error;
            });
        }
        async close() {
            if (!this.closed) {
                this.closed = true;
                await this.io.close().catch(error => {
                    throw error;
                });
                await this.proxy.close().catch(error => {
                    throw error;
                });
                await this.downloader.close().catch(error => {
                    throw error;
                });
                await this.db.close().catch(error => {
                    throw error;
                });
            }
        }
        callHelpResolver() {
            this.downloader.downloadVerified();
            if (this.helpFunction !== false) {
                this.helpFunction.call(this);
                this.helpFunction = false;
            }
        }
        parseRangeInput(referenceNumberRange) {
            const idRange = referenceNumberRange.split('-').map(i => Number(i));
            this.count = 0;
            this.ids = Utils_1.shuffle(Utils_1.range(idRange[0], idRange[1]));
            this.selectedId = this.ids[this.count].toString();
            this.lastResolve = 0;
        }
        async sleep() {
            const delay = Utils_1.rand(Application.config.delayRange[0], Application.config.delayRange[1]);
            con.log('wait for', delay);
            await Utils_1.sleep(delay * 1000);
        }
        async downloadNext(wait = true) {
            if (!this.userInterface.force) {
                while (true) {
                    let itemInfo = await Utils_1.findPDF(Number(this.selectedId));
                    if (itemInfo) {
                        con.log('file exist');
                        con.log(itemInfo.toJSON());
                        this.count++;
                        if (this.count === this.ids.length) {
                            await this.close();
                            break;
                        }
                        this.selectedId = this.ids[this.count].toString();
                    }
                    else
                        break;
                }
            }
            if (this.count < this.ids.length) {
                if (wait) {
                    await this.sleep();
                }
                con.log(`${this.count + 1} from ${this.ids.length} start downloading`, this.selectedId);
                await this.downloader.download(this.selectedId).catch(error => {
                    throw error;
                });
            }
            else {
                await this.close();
            }
        }
        async logDownload(resultInfo) {
            if (!this.userInterface.force) {
                const info = new DatabaseManager_1.DownloadedPDF();
                info.hostname = os_1.default.hostname();
                info.path = path_1.join(this.io.browserDownloadPath, resultInfo.batchId, resultInfo.id + ".pdf");
                info.referenceNumber = Number(resultInfo.id);
                info.user = os_1.default.userInfo().username;
                await info.save();
            }
            con.log(`${this.count + 1} from ${this.ids.length} download complete`, this.selectedId);
        }
        async checkResolve() {
            const now = Date.now();
            if (now - this.lastResolve < Application.resolverThreshold)
                return false;
            this.lastResolve = now;
            this.callHelpResolver();
            return true;
        }
        async doOcr(resultInfo) {
            con.log(`${this.count + 1} from ${this.ids.length} start OCR`, this.selectedId);
            const result = await this.ocr.readImage(path_1.join(this.io.PDFsTempPath, resultInfo.batchId, resultInfo.id + ".pdf"), this.selectedId);
            con.log('ocr done');
            try {
                await result.pedigree.save();
                await result.horse.save();
            }
            catch (e) {
                con.log(e.parent.message);
                con.log(e.message);
            }
            con.log(`${this.count + 1} from ${this.ids.length} your data saved`, this.selectedId);
        }
    }
    Application.config = {
        databaseConnectionQuery: 'sqlite:db.sqlite',
        downloadPath: 'tmp',
        port: 3000,
        delayRange: [5, 8],
        proxyBlackList: [],
        deepSpeech: {
            model: "",
            score: "",
            executable: ""
        }
    };
    Application.resolverThreshold = 50;
    return Application;
})();
exports.Application = Application;
