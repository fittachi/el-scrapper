import * as Sentry from '@sentry/node'

Sentry.init({dsn: 'https://121ee8733c5243ee8aa1c3b332f2fb7f@o138206.ingest.sentry.io/5257214'});
import os from "os";

Sentry.configureScope(function (scope) {
    scope.setUser({"username": os.hostname()});
});
import {Application} from "./libs/Application"

new Application()