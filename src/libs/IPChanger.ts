import {exec} from 'child_process'
import {Application} from "./Application";
import {logger} from './Utils'

const con = logger('Proxy')

export class IPChanger {

    protected off: boolean
    protected proxy: string

    constructor(off = false, proxy: string) {
        this.proxy = proxy
        this.off = off
    }

    protected serversList: Array<{ type: 'nordvpn' | 'expressvpn', name: string }> = []
    protected selectedServer: { type: 'nordvpn' | 'expressvpn', name: string } = {type: 'nordvpn', name: ''}

    protected static currentConnectedServerName: { type: 'nordvpn' | 'expressvpn', name: string } = {
        type: 'nordvpn',
        name: ''
    }

    protected loadExpressVpn() {
        return new Promise((resolve, reject) => {
            exec('expressvpn list all', (error, stdout) => {
                if (error) reject(error)
                let result = stdout.split(/\r?\n/)
                result.splice(0, 2)
                result = result.map(r => r.split(' ')[0]).filter(r => !Application.config.proxyBlackList.includes(r))
                if (result.length <= 0) {
                    reject(new Error(`VPN Servers list is empty,
                                        Its look like you are offline,
                                        please check output of "expressvpn list"
                                        
                                        expressvpn OUTPUT = 
                                        
                                        "${stdout}"
                                        `))
                }
                this.serversList = this.serversList.concat(result.map(r => ({type: 'expressvpn', name: r})))
                resolve(this.serversList)
            })
        })
    }

    protected loadNordVpn() {
        return new Promise((resolve, reject) => {
            exec('nordvpn countries', (error, stdout) => {
                if (error) reject(error)
                const result = stdout.trim().split(', ')
                result[0] = 'Albania'
                this.serversList = result.filter(r => !Application.config.proxyBlackList.includes(r)).map(r => ({
                    type: "nordvpn",
                    name: r
                }))
                if (this.serversList.length <= 0) {
                    reject(new Error(`VPN Servers list is empty,
                                        Its look like you are offline,
                                        please check output of "nordvpn countries"
                                        
                                        NORDVPN OUTPUT = 
                                        
                                        "${stdout}"
                                        `))
                }
                this.selectedServer = this.serversList[Math.floor(Math.random() * this.serversList.length)]
                resolve(this.selectedServer)
            })
        })
    }

    public async loadServersData() {
        if (!this.off) {
            if (this.proxy === 'both') {
                await this.loadNordVpn()
                await this.loadExpressVpn();
            } else if (this.proxy === 'nordvpn') {
                await this.loadNordVpn()
            } else if (this.proxy) {
                await this.loadExpressVpn();
            } else {
                throw new Error('wrong value for --proxy')
            }
        }
    }

    protected selectServer() {
        let debugCount = 0;
        let selectedServer = this.selectedServer
        do {
            let n = Math.floor(Math.random() * this.serversList.length - 2)
            selectedServer = this.serversList.filter(r => r !== this.selectedServer)[n]
            if (++debugCount === 5) {
                console.log(this.serversList.filter(r => r !== this.selectedServer))
                throw new Error("something wrong in Proxy unit n=" + n)
            }
        } while (typeof selectedServer === "undefined")
        this.selectedServer = selectedServer
    }

    public changeIP(): Promise<boolean> | void {
        if (!this.off) {
            this.selectServer()
            if (!this.selectedServer) {
                console.log(this.serversList)
            }
            return this.connect(this.selectedServer)
        } else {
            return new Promise(resolve => resolve(true))
        }
    }

    public connect(serverName: { type: 'nordvpn' | 'expressvpn', name: string }): Promise<boolean> {
        return new Promise((resolve, reject) => {
            con.log('try connect to proxy', this.selectedServer)
            exec(`nordvpn disconnect`)
            exec(`expressvpn disconnect`)
            setTimeout(() => {
                const command = exec(`${this.selectedServer.type} connect ${this.selectedServer.name}`)
                command.once('error', reject)
                command.once('exit', (exitCode) => {
                    if (exitCode === 0) {
                        IPChanger.currentConnectedServerName = serverName
                        if (this.selectedServer.type === 'expressvpn') {
                            return setTimeout(() => {
                                resolve(true)
                            }, 5000)
                        } else {
                            return resolve(true)
                        }
                    }
                    return resolve(false)
                })
            }, 1000)
        })
    }

    public static disconnect(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            const command = exec(`nordvpn disconnect`)
            command.once('error', reject)
            command.once('exit', (exitCode) => {
                if (exitCode === 0) {
                    IPChanger.currentConnectedServerName = {type: 'nordvpn', name: ''}
                    return resolve(true)
                }
                return resolve(false)
            })
        })
    }

    public async close(): Promise<void> {
        // await IPChanger.disconnect()
    }
}