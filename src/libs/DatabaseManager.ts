import {Sequelize, DataTypes, Model, Options} from 'sequelize'

export class Horse extends Model {
    public referenceNumber!: number
    public name!: string
    public foaledDate!: string
    public foaledIn!: string
    public type!: string
    public startedGame!: number
    public winner!: number
    public rawString!:string
    // public breeder!: string
    // public inbreeding!: string

    public static attributesType = {
        referenceNumber: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING
        },
        foaledDate: {
            type: DataTypes.STRING,
            allowNull: true
        },
        foaledIn: {
            type: DataTypes.STRING,
            allowNull: true
        },
        type: {
            type: DataTypes.STRING,
            allowNull: true
        },
        startedGame: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        winner: {
            type: DataTypes.INTEGER,
            allowNull: true
        },
        rawString :{
            type: DataTypes.STRING,
            allowNull: true
        }
        // breeder: {
        //     type: DataTypes.STRING,
        //     allowNull: true
        // },
        // inbreeding: {
        //     type: DataTypes.STRING,
        //     allowNull: true
        // }
    }
}

export class Pedigree extends Model {
    public referenceNumber!: number
    public S!: string
    public D!: string
    public SS!: string
    public SD!: string
    public DS!: string
    public DD!: string
    public SSS!: string
    public SSD!: string
    public SDS!: string
    public SDD!: string
    public DSS!: string
    public DSD!: string
    public DDS!: string
    public DDD!: string
    public SSSS!: string
    public SSSD!: string
    public SSDS!: string
    public SSDD!: string
    public SDSS!: string
    public SDSD!: string
    public SDDS!: string
    public SDDD!: string
    public DSSS!: string
    public DSSD!: string
    public DSDS!: string
    public DSDD!: string
    public DDSS!: string
    public DDSD!: string
    public DDDS!: string
    public DDDD!: string
    public SSSSS!: string
    public SSSSD!: string
    public SSSDS!: string
    public SSSDD!: string
    public SSDSS!: string
    public SSDSD!: string
    public SSDDS!: string
    public SSDDD!: string
    public SDSSS!: string
    public SDSSD!: string
    public SDSDS!: string
    public SDSDD!: string
    public SDDSS!: string
    public SDDSD!: string
    public SDDDS!: string
    public SDDDD!: string
    public DSSSS!: string
    public DSSSD!: string
    public DSSDS!: string
    public DSSDD!: string
    public DSDSS!: string
    public DSDSD!: string
    public DSDDS!: string
    public DSDDD!: string
    public DDSSS!: string
    public DDSSD!: string
    public DDSDS!: string
    public DDSDD!: string
    public DDDSS!: string
    public DDDSD!: string
    public DDDDS!: string
    public DDDDD!: string
    public static attributesType = {
        referenceNumber: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        S: {
            type: DataTypes.STRING
        },
        D: {
            type: DataTypes.STRING
        },
        SS: {
            type: DataTypes.STRING
        },
        SD: {
            type: DataTypes.STRING
        },
        DS: {
            type: DataTypes.STRING
        },
        DD: {
            type: DataTypes.STRING
        },
        SSS: {
            type: DataTypes.STRING
        },
        SSD: {
            type: DataTypes.STRING
        },
        SDS: {
            type: DataTypes.STRING
        },
        SDD: {
            type: DataTypes.STRING
        },
        DSS: {
            type: DataTypes.STRING
        },
        DSD: {
            type: DataTypes.STRING
        },
        DDS: {
            type: DataTypes.STRING
        },
        DDD: {
            type: DataTypes.STRING
        },
        SSSS: {
            type: DataTypes.STRING
        },
        SSSD: {
            type: DataTypes.STRING
        },
        SSDS: {
            type: DataTypes.STRING
        },
        SSDD: {
            type: DataTypes.STRING
        },
        SDSS: {
            type: DataTypes.STRING
        },
        SDSD: {
            type: DataTypes.STRING
        },
        SDDS: {
            type: DataTypes.STRING
        },
        SDDD: {
            type: DataTypes.STRING
        },
        DSSS: {
            type: DataTypes.STRING
        },
        DSSD: {
            type: DataTypes.STRING
        },
        DSDS: {
            type: DataTypes.STRING
        },
        DSDD: {
            type: DataTypes.STRING
        },
        DDSS: {
            type: DataTypes.STRING
        },
        DDSD: {
            type: DataTypes.STRING
        },
        DDDS: {
            type: DataTypes.STRING
        },
        DDDD: {
            type: DataTypes.STRING
        },
        SSSSS: {
            type: DataTypes.STRING
        },
        SSSSD: {
            type: DataTypes.STRING
        },
        SSSDS: {
            type: DataTypes.STRING
        },
        SSSDD: {
            type: DataTypes.STRING
        },
        SSDSS: {
            type: DataTypes.STRING
        },
        SSDSD: {
            type: DataTypes.STRING
        },
        SSDDS: {
            type: DataTypes.STRING
        },
        SSDDD: {
            type: DataTypes.STRING
        },
        SDSSS: {
            type: DataTypes.STRING
        },
        SDSSD: {
            type: DataTypes.STRING
        },
        SDSDS: {
            type: DataTypes.STRING
        },
        SDSDD: {
            type: DataTypes.STRING
        },
        SDDSS: {
            type: DataTypes.STRING
        },
        SDDSD: {
            type: DataTypes.STRING
        },
        SDDDS: {
            type: DataTypes.STRING
        },
        SDDDD: {
            type: DataTypes.STRING
        },
        DSSSS: {
            type: DataTypes.STRING
        },
        DSSSD: {
            type: DataTypes.STRING
        },
        DSSDS: {
            type: DataTypes.STRING
        },
        DSSDD: {
            type: DataTypes.STRING
        },
        DSDSS: {
            type: DataTypes.STRING
        },
        DSDSD: {
            type: DataTypes.STRING
        },
        DSDDS: {
            type: DataTypes.STRING
        },
        DSDDD: {
            type: DataTypes.STRING
        },
        DDSSS: {
            type: DataTypes.STRING
        },
        DDSSD: {
            type: DataTypes.STRING
        },
        DDSDS: {
            type: DataTypes.STRING
        },
        DDSDD: {
            type: DataTypes.STRING
        },
        DDDSS: {
            type: DataTypes.STRING
        },
        DDDSD: {
            type: DataTypes.STRING
        },
        DDDDS: {
            type: DataTypes.STRING
        },
        DDDDD: {
            type: DataTypes.STRING
        },
    }
}

export class DownloadedPDF extends Model {
    public referenceNumber!: number
    public hostname!: string
    public user!: string
    public path!: string

    public static attributesType = {
        referenceNumber: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        hostname: {
            type: DataTypes.STRING,
            allowNull: true
        },
        user: {
            type: DataTypes.STRING,
            allowNull: true
        },
        path: {
            type: DataTypes.STRING
        }
    }
}

export class NickingStats extends Model {
    public referenceNumber!: number
    public s_foals!: string
    public s_starters!: string
    public s_winners!: string
    public s_bw!: string
    public s_earnings!: string
    public s_aei!: string
    public bs_mares!: string
    public bs_foals!: string
    public bs_starters!: string
    public bs_winners!: string
    public bs_bw!: string
    public bs_earnings!: string
    public bs_aei!: string
    public bs_s_mares!: string
    public bs_s_foals!: string
    public bs_s_starters!: string
    public bs_s_winners!: string
    public bs_s_bw!: string
    public bs_s_earnings!: string
    public bs_s_aei!: string

    public static attributesType = {
        referenceNumber: {
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        s_foals: {
            type: DataTypes.STRING,
            allowNull: true
        },
        s_starters: {
            type: DataTypes.STRING,
            allowNull: true
        },
        s_winners: {
            type: DataTypes.STRING,
            allowNull: true
        },
        s_bw: {
            type: DataTypes.STRING,
            allowNull: true
        },
        s_earnings: {
            type: DataTypes.STRING,
            allowNull: true
        },
        s_aei: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_mares: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_foals: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_starters: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_winners: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_bw: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_earnings: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_aei: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_mares: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_foals: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_starters: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_winners: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_bw: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_earnings: {
            type: DataTypes.STRING,
            allowNull: true
        },
        bs_s_aei: {
            type: DataTypes.STRING,
            allowNull: true
        },
    }
}

export default class DatabaseManager {
    protected connection: Sequelize

    constructor(connectionQuery: string) {
        this.connection = new Sequelize(connectionQuery, {logging: false})
        Horse.init(Horse.attributesType, {
            sequelize: this.connection,
            timestamps: true
        })
        Horse.sync()
        Pedigree.init(Pedigree.attributesType, {
            sequelize: this.connection,
            timestamps: true
        })
        Pedigree.sync()
        DownloadedPDF.init(DownloadedPDF.attributesType, {
            sequelize: this.connection,
            timestamps: true
        })
        DownloadedPDF.sync()
        NickingStats.init(NickingStats.attributesType, {
            sequelize: this.connection,
            timestamps: true
        })
        NickingStats.sync()
    }

    public connect() {
        return this.connection.authenticate();
    }

    protected disconnect() {
        return this.connection.close()
    }

    public async close() {
        this.disconnect()
    }


}