import * as fs from "fs"
import * as Path from "path"
import {By, ThenableWebDriver, WebElement, until} from "selenium-webdriver";
import {PNG as png} from 'pngjs'
import {DownloadedPDF} from "./DatabaseManager";
import {Actions} from "selenium-webdriver/lib/input";
import {get} from 'https'

const con = logger('AnimationSimulator')

export function groupBy<S>(list: Array<S>, keyGetter: (s: S) => string): { [key: string]: Array<S> } {
    const map: { [key: string]: Array<S> } = {};
    list.forEach((item) => {
        const key: string = keyGetter(item);
        if (!map.hasOwnProperty(key)) {
            map[key] = [];
        }
        map[key].push(item);
    });
    return map;
}

export function sleep(timeout: number): Promise<void> {
    return new Promise(r => setTimeout(r, timeout))
}

export function range(start: number, end: number) {
    return [...Array(end - start).keys()].map(r => r + start)
}

export function shuffle<S>(array: Array<S>): Array<S> {
    return (array.map(a => [Math.random(), a]) as Array<[number, S]>).sort((a, b) => a[0] - b[0]).map((a) => a[1])
}

export function rand(min: number | [number, number], max?: number) {
    if (Array.isArray(min)) {
        return Math.floor(Math.random() * (min[1] - min[0])) + min[0]
    } else {
        if (max) {
            return Math.floor(Math.random() * (max - min)) + min
        } else throw new Error('not enough data')
    }
}

export function dropSimilar(arr: Array<number>, threshold: number = 1): Array<number> {
    return arr.filter((item, index, self) => Math.abs(self[index + 1] - item) > threshold || isNaN(self[index + 1]))
}

export function deleteFolderRecursive(path: string) {
    if (fs.existsSync(path)) {
        fs.readdirSync(path).forEach((file, index) => {
            const curPath = Path.join(path, file);
            if (fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
}

export function findElByTimeOut(driver: ThenableWebDriver, by: By, timeout: number = 12500): Promise<WebElement> {
    return driver.wait(until.elementLocated(by), timeout)
}

export function diffImage(imageBuf: Buffer, imageBuf2: Buffer): Array<{ x: number, y: number, diff: number }> {
    const threshold = 5
    const image1 = png.sync.read(imageBuf)
    const image2 = png.sync.read(imageBuf2)
    if (image1.width !== image2.width || image1.height !== image2.height) {
        throw new Error("images size not same")
    }
    const results = []
    for (let y = 0; y < image1.height; y++) {
        for (let x = 0; x < image1.width; x++) {
            let idx = (image1.width * y + x) << 2;
            const pix1 = (image1.data[idx] + image1.data[idx + 1] + image1.data[idx + 2]) / 3 * (image1.data[idx + 3] / 255)
            const pix2 = (image2.data[idx] + image2.data[idx + 1] + image2.data[idx + 2]) / 3 * (image2.data[idx + 3] / 255)
            if (Math.abs(pix1 - pix2) >= threshold) {
                results.push({x, y, diff: pix2 - pix1})
            }
        }
    }
    return results
}

export function findPDF(refID: number) {
    return DownloadedPDF.findOne({
        where: {
            referenceNumber: refID
        }
    })
}

export function animationAction(action: Actions,
                                start: { x: number, y: number },
                                end: { x: number, y: number }, duration: number = 1000) {
    con.log(`go for ${JSON.stringify(start)} to ${JSON.stringify(end)}`)
    const threshold = 8
    action = action.move(start)
    const it = duration / threshold
    const deltaX = (end.x - start.x) / it
    const deltaY = (end.y - start.y) / it
    for (let i = 0; i < it; i++) {
        start.x += deltaX
        start.y += deltaY
        action = action
            .move({
                x: Math.floor(start.x),
                y: Math.floor(start.y),
            })
        // .pause(threshold)
    }
    action = action.move(end)
    return action
}

export function downloadContent(path: string, url: string) {
    return new Promise(resolve => {
        const file = fs.createWriteStream(path)
        file.once('finish', resolve)
        get(url, (response) => response.pipe(file))
    })
}

export function logger(scopeName: string) {
    return {
        log(ref: any, message?: any) {
            if (message)
                console.log(Date.now() / 1000, scopeName, ref, message)
            else
                console.log(Date.now() / 1000, scopeName, ref)
        }
    }
}