import {ProgramConfig, UserInterface} from "./UserInterface"
import {Scrapper} from "./Scrapper"
import {IOManager} from "./IOManager"
import {join, parse, extname} from 'path'
import {OCRManager} from "./OCRManager"
import {range, shuffle, sleep, rand, findPDF, logger} from './Utils'
import DatabaseManager, {DownloadedPDF, NickingStats} from "./DatabaseManager"
import {IPChanger} from "./IPChanger"
import os from 'os'
import glob from 'glob'
import Chunk from 'lodash.chunk'
import {promises} from "fs"

type resolveFunctionType = () => void

const con = logger('Application')

export class Application {
    public static config: ProgramConfig = {
        databaseConnectionQuery: 'sqlite:db.sqlite',
        downloadPath: 'tmp',
        port: 3000,
        delayRange: [5, 8],
        proxyBlackList: [],
        deepSpeech: {
            model: "",
            score: "",
            executable: ""
        }
    }
    protected userInterface: UserInterface
    protected io!: IOManager
    protected downloader!: Scrapper
    protected ocr!: OCRManager
    protected db!: DatabaseManager
    protected proxy!: IPChanger
    protected helpFunction: resolveFunctionType | false = false

    protected selectedId: string = ''
    protected count: number = 0
    protected ids: Array<number> = []
    protected lastResolve: number = 0
    protected closed: boolean = false

    private static readonly resolverThreshold = 50

    constructor() {
        this.userInterface = UserInterface.build()
            .on('config:ready', async (config: ProgramConfig) => {
                Application.config = config
            })
            .on('download', async (ref: string) => {
                this.ids = [Number(ref)]
                this.selectedId = ref.toString()
                await this.init().catch(error => {
                    throw error
                })
                await this.initBrowser().catch(error => {
                    throw error
                })
                this.io.once('resolve', async () => {
                    this.callHelpResolver()
                    const resultInfo = await this.io.prepareFile(ref)
                    await this.logDownload(resultInfo).catch(error => {
                        throw error
                    })
                    await this.close().catch(error => {
                        throw error
                    })
                })
                setTimeout(this.downloadNext.bind(this, false))
            })
            .on('ocr', async (fileAddress: string) => {
                await this.init().catch(error => {
                    throw error
                })

                const path = parse(fileAddress)
                if (!this.userInterface.force) {
                    let tp = await NickingStats.findOne({
                        where: {
                            referenceNumber: Number(path.name)
                        }
                    })
                    if (tp) {
                        con.log(`it seems that this file (#${path.name}) has been added to database before`)
                        await this.close()
                        process.exit(1)
                    }
                }
                const result = await this.ocr.readImage(fileAddress, path.name, false).catch(error => {
                    throw error
                })


                try {
                    await result.pedigree.save()
                    await result.horse.save()
                    await result.nickingStats.save()
                } catch (e) {
                    con.log(e.parent.message)
                    con.log(e.message)
                } finally {
                    await this.close().catch(error => {
                        throw error
                    })
                }

                con.log('data saved to database', fileAddress)
            })
            .on('save', async (ref: string) => {
                this.ids = [Number(ref)]
                this.selectedId = ref.toString()
                await this.init().catch(error => {
                    throw error
                })
                await this.initBrowser().catch(error => {
                    throw error
                })
                this.io.once('resolve', async () => {
                    this.callHelpResolver()
                    const resultInfo = await this.io.prepareFile(ref)
                    await this.logDownload(resultInfo).catch(error => {
                        throw error
                    })
                    await this.doOcr(resultInfo).catch(error => {
                        throw error
                    })
                    await this.close().catch(error => {
                        throw error
                    })
                })
                setTimeout(this.downloadNext.bind(this, false))
            })
            .on('save:bulk', async (referenceNumberRange) => {
                await this.init().catch(error => {
                    throw error
                })
                this.parseRangeInput(referenceNumberRange)
                await this.initBrowser().catch(error => {
                    throw error
                })
                this.io.on('resolve', async () => {
                    if (!await this.checkResolve()) return
                    const resultInfo = await this.io.prepareFile(this.selectedId)
                    await this.logDownload(resultInfo).catch(error => {
                        throw error
                    })
                    await this.doOcr(resultInfo).catch(error => {
                        throw error
                    })
                    this.count++
                    this.selectedId = (this.ids[this.count] || '').toString()
                    await this.downloadNext().catch(error => {
                        throw error
                    })
                })
                setTimeout(this.downloadNext.bind(this, false))
            })
            .on('download:bulk', async (referenceNumberRange) => {
                await this.init().catch(error => {
                    throw error
                })
                this.parseRangeInput(referenceNumberRange)
                await this.initBrowser().catch(error => {
                    throw error
                })
                this.io.on('resolve', async () => {
                    if (!await this.checkResolve()) return
                    const resultInfo = await this.io.prepareFile(this.selectedId)
                    await this.logDownload(resultInfo).catch(error => {
                        throw error
                    })
                    this.count++
                    this.selectedId = (this.ids[this.count] || '').toString()
                    await this.downloadNext().catch(error => {
                        throw error
                    })
                })
                setTimeout(this.downloadNext.bind(this, false))
            })
            .on('register:pdfs', async pattern => {
                await this.init()
                glob(pattern, async (er, files) => {
                    if (er) throw er
                    for (let item of files) {
                        const id = Number(parse(item).name)
                        let info = await DownloadedPDF.findOne({
                            where: {
                                referenceNumber: Number(id)
                            }
                        })
                        if (!info) {
                            info = new DownloadedPDF()
                        }
                        info.hostname = os.hostname()
                        info.path = join(process.cwd(), item)
                        info.referenceNumber = id
                        info.user = os.userInfo().username
                        await info.save()
                        con.log(`#${parse(item).name} added to database`)
                    }
                    await this.close().catch(error => {
                        throw error
                    })
                })
            })
            .on('category:pdfs', async ({pattern, destination}) => {
                glob(pattern, async (er, files) => {
                    if (er) throw er
                    files = files
                        .filter(r => !!extname(r))
                        .sort((a, b) => a > b ? -1 : 1)
                    const chunks = Chunk(files, 1000)
                    chunks.forEach(async (chunk, index) => {
                        console.log('start', index)
                        const path = join(destination, index.toString())
                        await promises.mkdir(path)
                        for (let file of chunk) {
                            const info = parse(file)
                            await promises.rename(file, join(path, info.base))
                        }
                        console.log('end', index)
                    })
                })
            })
            .parseArrangement()
    }

    async init() {
        this.io = new IOManager(Application.config.downloadPath)
        await this.io.prepareMainPath().catch(error => {
            throw error
        })
        this.io.registerBrowserWatcher()


        this.db = new DatabaseManager(Application.config.databaseConnectionQuery)
        this.ocr = new OCRManager(this.io.OCRTempPath, this.userInterface.force)


        this.proxy = new IPChanger(this.userInterface.dontUseProxy, this.userInterface.proxy)
        await this.proxy.loadServersData().catch(error => {
            throw error
        })

        this.downloader = new Scrapper(this.io.browserDownloadPath, Application.config.port)
        this.downloader.setProxy(this.proxy)
        this.downloader
            .on("needHelp", (r) => {
                this.helpFunction = r
                this.userInterface.notify({
                    title: "Scrapper need help",
                    message: "Please solve the puzzle."
                })
                con.log("scrapper need help")
            })
            .on('setHelp', r => this.helpFunction = r)


        process.once('SIGINT', async () => {
            con.log("Caught interrupt signal");
            await this.close()
            process.exit(1)
        });
    }

    protected async initBrowser() {
        try {
            while (!await this.proxy.changeIP()) {
            }
        } catch (e) {
            con.log(e.message)
        }
        await this.downloader.initializeServer().catch(error => {
            throw error
        })
        await this.downloader.initializeDriver("chrome").catch(error => {
            throw error
        })
    }

    async close() {
        if (!this.closed) {
            this.closed = true
            await this.io.close().catch(error => {
                throw error
            })
            await this.proxy.close().catch(error => {
                throw error
            })
            await this.downloader.close().catch(error => {
                throw error
            })
            await this.db.close().catch(error => {
                throw error
            })
        }
    }

    protected callHelpResolver() {
        this.downloader.downloadVerified()
        if (this.helpFunction !== false) {
            this.helpFunction.call(this)
            this.helpFunction = false
        }
    }

    protected parseRangeInput(referenceNumberRange: string) {
        const idRange = referenceNumberRange.split('-').map(i => Number(i))
        this.count = 0
        this.ids = shuffle(range(idRange[0], idRange[1]))
        this.selectedId = this.ids[this.count].toString()
        this.lastResolve = 0
    }

    protected async sleep() {
        const delay = rand(Application.config.delayRange[0], Application.config.delayRange[1])
        con.log('wait for', delay)
        await sleep(delay * 1000)
    }

    protected async downloadNext(wait: boolean = true) {
        if (!this.userInterface.force) {
            while (true) {
                let itemInfo = await findPDF(Number(this.selectedId))
                if (itemInfo) {
                    con.log('file exist')
                    con.log(itemInfo.toJSON())
                    this.count++
                    if (this.count === this.ids.length) {
                        await this.close()
                        break
                    }
                    this.selectedId = this.ids[this.count].toString()
                } else break
            }
        }
        if (this.count < this.ids.length) {
            if (wait) {
                await this.sleep()
            }
            con.log(`${this.count + 1} from ${this.ids.length} start downloading`, this.selectedId)
            await this.downloader.download(this.selectedId).catch(error => {
                throw error
            })
        } else {
            await this.close()
        }
    }

    protected async logDownload(resultInfo: { batchId: string, id: string }) {
        if (!this.userInterface.force) {
            const info = new DownloadedPDF()
            info.hostname = os.hostname()
            info.path = join(this.io.browserDownloadPath, resultInfo.batchId, resultInfo.id + ".pdf")
            info.referenceNumber = Number(resultInfo.id)
            info.user = os.userInfo().username
            await info.save()
        }
        con.log(`${this.count + 1} from ${this.ids.length} download complete`, this.selectedId)
    }

    protected async checkResolve(): Promise<boolean> {
        const now = Date.now()
        if (now - this.lastResolve < Application.resolverThreshold) return false
        this.lastResolve = now
        this.callHelpResolver()
        return true
    }

    protected async doOcr(resultInfo: { batchId: string, id: string }) {
        con.log(`${this.count + 1} from ${this.ids.length} start OCR`, this.selectedId)
        const result = await this.ocr.readImage(join(this.io.PDFsTempPath, resultInfo.batchId, resultInfo.id + ".pdf"), this.selectedId)
        con.log('ocr done')
        try {
            await result.pedigree.save()
            await result.horse.save()
        } catch (e) {
            con.log(e.parent.message)
            con.log(e.message)
        }
        con.log(`${this.count + 1} from ${this.ids.length} your data saved`, this.selectedId)
    }
}
