import {By, ThenableWebDriver, Key} from "selenium-webdriver";
import {findElByTimeOut, diffImage, sleep, animationAction, downloadContent, logger} from "./Utils";
import {join} from "path";
import {exec} from "child_process"
import {EOL} from 'os'
import fs from 'fs'
import {Application} from "./Application";


const numbers = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

const con = logger('GeeTestPuzzleCaptchaSolver')

export default class GeeTestPuzzleCaptchaSolver {
    protected driver: ThenableWebDriver

    constructor(driver: ThenableWebDriver) {
        this.driver = driver
    }

    public async activeTest() {
        let end
        while (true) {
            try {
                let {width, height} = await this.driver.manage().window().getRect();
                width -= 250
                height -= 250
                const initBTN = await findElByTimeOut(this.driver, By.className('geetest_radar_tip'))
                let offset = await initBTN.getRect()
                let action = this.driver.actions()
                let start = {
                    x: 0,
                    y: 0
                }
                end = {
                    x: 0,
                    y: 0
                }
                for (let i = 0; i < 3; i++) {
                    end = {
                        x: 50 + Math.floor(Math.random() * width),
                        y: 50 + Math.floor(Math.random() * height)
                    }
                    action = animationAction(action, start, end, Math.floor(Math.random() * 150) + 150)
                    action = action.pause(100);
                    start = end;
                }
                end = {
                    x: Math.floor(offset.x + 2),
                    y: Math.floor(offset.y + 3)
                }
                action = animationAction(action, start, end, Math.floor(Math.random() * 150) + 150).pause(150).click();
                await action.perform();
                break
            } catch (e) {

            }
        }
        con.log('done part one')
        return end
    }

    public loadImages(): Promise<Array<{ x: number, y: number, diff: number }>> {
        return new Promise((resolve, reject) => {
            setTimeout(async () => {
                try {
                    const fullBg = await findElByTimeOut(this.driver, By.className("geetest_canvas_fullbg"))
                    const bg = await findElByTimeOut(this.driver, By.className("geetest_canvas_bg"))
                    await sleep(1500)
                    const fullBgBase64 = await this.driver.executeScript("return arguments[0].toDataURL('image/png').substring(21);", fullBg) as string
                    const bgBase64 = await this.driver.executeScript("return arguments[0].toDataURL('image/png').substring(21);", bg) as string
                    let diff = diffImage(Buffer.from(fullBgBase64, 'base64'), Buffer.from(bgBase64, 'base64'))
                    resolve(diff)
                } catch (e) {
                    reject(e)
                }
            }, 1000)
        })
    }

    public async solve(diff: Array<{ x: number, y: number, diff: number }>, pos: { x: number, y: number }) {
        await sleep(1000)
        diff = diff.sort((a, b) => a.x - b.x)
        if (!diff[0]) {
            con.log('ops')
        }
        const value = diff[0].x + 7
        await sleep(500)
        const sliderBtn = await findElByTimeOut(this.driver, By.className('geetest_slider_button'))
        let offset = await sliderBtn.getRect()
        const startPos = {x: Math.floor(offset.x) + 15, y: Math.floor(offset.y) + 15}
        const endPos = {
            y: Math.floor(offset.y) + Math.floor(Math.random() * 20),
            x: Math.floor(offset.x) + Math.floor(value) + Math.floor(Math.random() * 2) * (Math.random() < 0.5 ? -1 : 1)
        }
        let action = this.driver.actions().move(pos).pause(50)
        action = animationAction(action, pos, startPos).pause(150)
        action = action.press()
        action = animationAction(action, startPos, endPos)
        await action.pause(50).release().perform()
        con.log('done part two')
    }

    protected convertAudio(inputPath: string, outPutPath: string) {
        return new Promise(((resolve, reject) => {
            setTimeout(async () => {
                const command = exec(`ffmpeg -i ${inputPath} -acodec pcm_s16le -ac 1 -ar 16000 ${outPutPath}`)
                command.on('error', reject)
                command.on('exit', code => {
                    if (code === 0) resolve(outPutPath)
                    else reject()
                })
            }, 500)
        }))
    }

    public static ATT(inputPath: string): Promise<string> {
        return new Promise((resolve, reject) => {
            exec(`${Application.config.deepSpeech.executable} --model ${Application.config.deepSpeech.model} --scorer ${Application.config.deepSpeech.score} --audio ${inputPath}`,
                (error, stdout, stderr) => {
                    if (error) reject(error)
                    const code = stdout.split(EOL)[0].split(' ')
                        .map((r: string) => numbers.findIndex(d => d === r.toString().toLowerCase()).toString())
                        .filter(r => r !== '-1').join('')
                    resolve(code)
                })
        })
    }

    public async audioSolver(pos: { x: number, y: number }) {
        const audioBtn = await findElByTimeOut(this.driver, By.className('geetest_voice'))
        const offset = await audioBtn.getRect()
        let action = this.driver.actions().move(pos).pause(50)
        await animationAction(action, pos, {
            x: Math.floor(offset.x),
            y: Math.floor(offset.y)
        }).pause(50).click().perform()
        const audio = await findElByTimeOut(this.driver, By.className('geetest_music'), 20 * 1000)
        let audioUrl
        await sleep(4000)
        while (!(typeof audioUrl === 'string' && audioUrl.toString().length > 0)) {
            await sleep(1000)
            audioUrl = await this.driver.executeScript("return arguments[0].currentSrc;", audio) as string
        }
        try {
            await fs.promises.unlink(join(__dirname, 'text.mp3'))
            await fs.promises.unlink(join(__dirname, 'text.wav'))
        } catch (e) {
            // ist ok
        }
        await downloadContent(join(__dirname, 'text.mp3'), audioUrl)
        con.log('original song downloaded at ', join(__dirname, 'text.mp3'))
        await this.convertAudio(join(__dirname, 'text.mp3'), join(__dirname, 'text.wav'))
        con.log('converted song save at ', join(__dirname, 'text.mp3'))
        const code = await GeeTestPuzzleCaptchaSolver.ATT(join(__dirname, 'text.wav'))
        con.log('code is ', code)
        const input = await findElByTimeOut(this.driver, By.className('geetest_input'))
        await input.sendKeys(code)
        await sleep(150)
        await input.sendKeys(Key.RETURN);
    }
}