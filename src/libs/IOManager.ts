import fs from "fs"
import path from "path"
import {EventEmitter} from "events"


export declare interface IOManager {
    on(event: 'resolve', listener: () => void): this;

    on(event: 'pdf:ready', listener: (resultInfo: { batchId: string, id: number | string }) => void): this;
}

export class IOManager extends EventEmitter {
    public static readonly BrowserTmpPrefix = 'browser'
    public static readonly PDFsTmpPrefix = 'PDFs'
    public static readonly OCRTmpPrefix = 'OCRTmp'
    public static readonly defaultPDFName = 'Free-5X-Pedigree.pdf'
    public static readonly batchSize = 100

    public readonly path: string;
    protected browserPath: string;
    protected PDFsPath: string;
    protected OCRPath: string;
    protected browserPathWatcher!: fs.FSWatcher;
    protected batch: Array<string> = []
    protected isBatchChecked: boolean = false

    protected async fixPath(pathAddress: string): Promise<void> {
        if (fs.existsSync(pathAddress)) {
            if (await fs.promises.stat(pathAddress).then(r => !r.isDirectory())) {
                throw new Error(`${pathAddress} in not Directory`)
            }
        } else {
            await fs.promises.mkdir(pathAddress)
        }
    }

    protected async addBatch(id: string): Promise<void> {
        if (!fs.existsSync(path.join(this.PDFsPath, id))) {
            try {
                await fs.promises.mkdir(path.join(this.PDFsPath, id))
            } catch (e) {
                console.log(e)
                process.exit(1)
            }
        }
        this.batch.unshift(id)
    }

    protected async updateBatchs(): Promise<void> {
        this.batch = await fs.promises.readdir(this.PDFsPath, {withFileTypes: true})
            .then(dir => dir.filter(d => d.isDirectory()).map(d => d.name).sort().reverse())
        if (this.batch.length === 0) {
            await this.addBatch("1")
        }
    }

    get browserDownloadPath() {
        return this.browserPath
    }

    get OCRTempPath() {
        return this.OCRPath
    }

    get PDFsTempPath() {
        return this.PDFsPath
    }

    protected async checkBatch(id: string): Promise<boolean> {
        if (!id) return true
        return fs.promises.readdir(path.join(this.PDFsPath, id)).then(result => result.length < IOManager.batchSize)
    }

    public async getBatch(): Promise<string> {
        if (!await this.checkBatch(this.batch[0])) {
            this.addBatch((Number(this.batch[0]) + 1).toString())
        }
        return this.batch[0]
    }

    protected browserPathChangeHandler(eventType: string, filename: string): void {
        if (eventType === 'rename' && filename === IOManager.defaultPDFName) {
            if (fs.existsSync(path.join(this.browserPath, IOManager.defaultPDFName))) {
                this.emit('resolve')
            }
        }
    }

    constructor(tmpPath: string) {
        super()
        const tmp = path.resolve(path.join(__dirname, '..', '..'), path.normalize(tmpPath))
        fs.accessSync(tmp, fs.constants.R_OK | fs.constants.W_OK);
        this.path = tmp
        this.browserPath = path.join(this.path, IOManager.BrowserTmpPrefix)
        this.PDFsPath = path.join(this.path, IOManager.PDFsTmpPrefix)
        this.OCRPath = path.join(this.path, IOManager.OCRTmpPrefix)
    }

    public async prepareMainPath(): Promise<void> {
        await this.fixPath(this.browserPath)
        await this.fixPath(this.PDFsPath)
        await this.fixPath(this.OCRPath)
        await this.fixDownloadPath()
        await this.updateBatchs()
    }

    protected async fixDownloadPath() {
        if (fs.existsSync(path.join(this.browserPath, IOManager.defaultPDFName))) {
            await fs.promises.unlink(path.join(this.browserPath, IOManager.defaultPDFName))
        }
    }

    public registerBrowserWatcher(): this {
        this.browserPathWatcher = fs.watch(this.browserPath, this.browserPathChangeHandler.bind(this))
        return this
    }

    public prepareFile(id: string): Promise<{ batchId: string, id: string }> {
        return new Promise(async resolve => {
            const batchId = await this.getBatch()
            const finalAddress = path.join(this.PDFsPath, batchId, `${id}.pdf`)
            setTimeout(async () => {
                try {

                    await fs.promises.rename(path.join(this.browserPath, IOManager.defaultPDFName), finalAddress)

                } catch (e) {
                    if (e.errno !== -2) {
                        console.log(e)
                        process.exit(1)
                    }
                }
                this.emit('pdf:ready', {batchId, id})
                resolve({batchId, id})
            }, 5)
        })
    }

    public async close(): Promise<void> {
        if (this.browserPathWatcher) {
            this.browserPathWatcher.close()
        }
        const data = await fs.promises.readdir(this.browserDownloadPath)
        for (const file in data) {
            if (file !== '.' && file !== '..') {
                await fs.promises.unlink(file)
            }
        }
    }
}