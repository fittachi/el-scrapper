import {notify, Notification} from 'node-notifier'
import {Command} from 'commander'
import {readFileSync, existsSync} from "fs"
import {EventEmitter} from "events"


export interface ProgramConfig {
    readonly downloadPath: string;
    readonly databaseConnectionQuery: string;
    readonly port: number;
    readonly delayRange: [number, number];
    readonly proxyBlackList: Array<string>
    readonly deepSpeech: {
        model: string;
        score: string;
        executable: string;
    }
}

export declare interface UserInterface {
    on(event: 'save', listener: (referenceNumber: string) => void): this;

    on(event: 'download', listener: (referenceNumber: string) => void): this;

    on(event: 'ocr', listener: (fileAddress: string) => void): this;

    on(event: 'save:bulk', listener: (referenceNumberRange: string) => void): this;

    on(event: 'download:bulk', listener: (referenceNumberRange: string) => void): this;

    on(event: 'register:pdfs', listener: (pattern: string) => void): this;

    on(event: 'category:pdfs', listener: ({pattern, destination}: { pattern: string, destination: string }) => void): this;

    on(event: 'config:ready', listener: (config: ProgramConfig) => void): this;
}

export class UserInterface extends EventEmitter {
    protected argsManager = new Command()
    protected programConfig!: ProgramConfig

    private constructor() {
        super()
        this.argsManager
            .version("version 0.8.0", "-v, --version", "show program version")
            .description("Equineline Pedigree scrapper")
            .option("-c, --config-file <pathToConfigFile>", "config file for program")
            .option("--dont-use-proxy", "dont change IP")
            .option("--proxy <proxy>", "change vpn provider", "both")
            .option('--force', 'force to update')

        this.argsManager
            .command('save <referenceNumber>')
            .description("download OCR and save an equineline document")
            .action((referenceNumber: string) => {
                setTimeout(() => this.emit('save', referenceNumber))
            })
        this.argsManager
            .command('download <referenceNumber>')
            .description("download an equineline document")
            .action((referenceNumber: string) => {
                setTimeout(() => this.emit('download', referenceNumber))
            })
        this.argsManager
            .command('OCR <fileAddress>')
            .description("OCR an equineline document")
            .action((fileAddress: string) => {
                setTimeout(() => this.emit('ocr', fileAddress))
            })
        // bulk
        this.argsManager
            .command('download-bulk <referenceNumberRange>')
            .description("download equineline documents EX: 150-2000")
            .action((referenceNumberRange: string) => {
                setTimeout(() => this.emit('download:bulk', referenceNumberRange))
            })
        this.argsManager
            .command('save-bulk <referenceNumberRange>')
            .description("download OCR and save equineline documents EX: 150-2000")
            .action((referenceNumberRange: string) => {
                setTimeout(() => this.emit('save:bulk', referenceNumberRange))
            })
        this.argsManager
            .command('register-pdfs <pattern>')
            .description('register pdfs to database as downloaded ex: ./tmp/PDFs/**/*.pdf')
            .action((pattern: string) => {
                setTimeout(() => this.emit('register:pdfs', pattern))
            })
        this.argsManager
            .command('category-pdfs <pattern> <destination>')
            .description('move pdfs to destination as 1000 chunks ex: "./tmp/PDFs/**/*.pdf" "./dist"')
            .action((pattern: string, destination: string) => {
                setTimeout(() => this.emit('category:pdfs', {pattern, destination}))
            })
    }


    public notify(config: Notification) {
        notify(config)
    }

    public readConfigFile(path: string) {
        if (!existsSync(path)) {
            throw new Error(`Config file not found config path: "${path}"`)
        }
        const tmp = JSON.parse(readFileSync(path).toString())
        this.programConfig = tmp as ProgramConfig
        this.emit('config:ready', this.programConfig)
    }

    public parseArrangement(): UserInterface {
        this.argsManager.parse(process.argv)
        const configPath = this.argsManager.configFile ? this.argsManager.configFile : "config.json"
        this.readConfigFile(configPath)
        return this
    }

    get config() {
        return this.programConfig
    }

    public static build() {
        return new UserInterface()
    }

    get dontUseProxy() {
        return this.argsManager.dontUseProxy
    }

    get proxy() {
        return this.argsManager.proxy
    }

    get force() {
        return this.argsManager.force
    }

}