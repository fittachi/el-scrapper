import {IPChanger} from "./IPChanger"

require('chromedriver')
require('geckodriver')
import * as chrome from "selenium-webdriver/chrome"
import * as firefox from "selenium-webdriver/firefox"
import {Builder, By, ThenableWebDriver} from 'selenium-webdriver'
import {EventEmitter} from "events"
import {createServer, Server} from 'http'
import GeeTestPuzzleCaptchaSolver from "./GeeTestPuzzleCaptchaSolver";
import {sleep, logger} from "./Utils";

const con = logger('downloader')

export enum ScrapperStatus {
    ok,
    captchaLevel1,
    captchaLevel2,
    loadingError,
    unknown
}

export class ScrapperBuilder {
    private browserName: 'chrome' | 'firefox' = 'firefox'
    private downloadPathAddress!: string
    private baseServerPort: number = 3000

    public static instance() {
        return new ScrapperBuilder()
    }

    public browser(browserName: 'chrome' | 'firefox') {
        this.browserName = browserName
        return this
    }

    public downloadPath(downloadPath: string) {
        this.downloadPathAddress = downloadPath
        return this
    }

    public internalServerPort(portNumber: number) {
        this.baseServerPort = portNumber
        return this
    }

    public async build() {
        const scrapper = new Scrapper(this.downloadPathAddress, this.baseServerPort)
        await scrapper.initializeDriver(this.browserName)
        await scrapper.initializeServer()
        return scrapper
    }
}

export declare interface Scrapper {
    on(event: 'needHelp', listener: (resolveFunction: () => void) => void): this;

    on(event: 'setHelp', listener: (resolveFunction: () => void) => void): this;

    on(event: 'status', listener: (status: ScrapperStatus) => void): this;

    on(event: 'downloadCompleted', listener: (referenceNumber: string | number) => void): this;
}

export class Scrapper extends EventEmitter {
    protected driver!: ThenableWebDriver
    protected downloadPath: string
    protected baseServerPort: number
    protected baseServer!: Server
    protected logReferenceNumber: string = "0"
    protected proxy!: IPChanger
    protected timeout: NodeJS.Timeout | false = false
    protected failedTryCount: { count: number, ref: string } = {count: 0, ref: "-1"}

    constructor(downloadPath: string, baseServerPort: number) {
        super()
        this.baseServerPort = baseServerPort
        this.downloadPath = downloadPath
    }

    protected createFireFox(): ThenableWebDriver {
        const browserOptions = new firefox.Options()
        browserOptions.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf");
        browserOptions.setPreference("browser.download.folderList", 2);
        browserOptions.setPreference("browser.download.manager.showWhenStarting", true);
        browserOptions.setPreference("browser.download.dir", this.downloadPath);
        browserOptions.setPreference("plugin.scan.plid.all", false);
        browserOptions.setPreference("plugin.scan.Acrobat", "99.0");
        browserOptions.setPreference("pdfjs.disabled", true);
        return new Builder()
            .forBrowser('firefox')
            .setFirefoxOptions(browserOptions)
            .build();
    }

    protected createChrome(): ThenableWebDriver {
        return new Builder()
            .forBrowser('chrome')
            .setChromeOptions(new chrome.Options().setUserPreferences({
                "download.prompt_for_download": false,
                "download.default_directory": this.downloadPath,
                "download.directory_upgrade": true,
                "plugins.always_open_pdf_externally": true,
                "download.extensions_to_open": "applications/pdf"
            }))
            .build();
    }

    protected needHelp(resolver?: () => void) {
        if (resolver) {
            return this.emit('needHelp', resolver)
        }
        return new Promise(r => this.emit('needHelp', r))
    }

    protected captchaLevelOneHandler() {
        return new Promise(async resolve => {
            this.emit('setHelp', resolve)
            const solver = new GeeTestPuzzleCaptchaSolver(this.driver);
            setTimeout(async () => {
                let count = 0
                let e;
                while (true) {
                    try {
                        count++
                        if (count === 3) {
                            con.log('Error', e.message)
                            break
                        }
                        const pos = await solver.activeTest()
                        await solver.audioSolver(pos)
                        break
                    } catch (err) {
                        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`)
                        await sleep(3000)
                        await this.driver.navigate().to(this.getQuery(this.logReferenceNumber))
                        e = err
                    }
                }

            }, 2500)
            // this.emit('needHelp', resolve)
        })
    }

    public setProxy(proxy: IPChanger) {
        this.proxy = proxy
    }

    protected async captchaLevelTwoHandler(referenceNumber: number | string) {
        await this.driver.close()
        await this.proxy.changeIP()
        await this.initializeDriver('chrome')
        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`)
        await sleep(2000)
        await this.download(this.logReferenceNumber)
    }

    protected reloadPage(referenceNumber: number | string) {
        con.log('timeout or reload call for ', referenceNumber)
        return new Promise(async resolve => {
            await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`)
            setTimeout(async () => {
                await this.download(referenceNumber)
                resolve()
            }, 5000)
        })
    }

    protected async checkStatus(): Promise<ScrapperStatus> {
        await sleep(1000)
        let status: ScrapperStatus = ScrapperStatus.unknown
        try {
            await this.driver.findElement(By.id('display-pages'))
            status = ScrapperStatus.captchaLevel1
        } catch (e) {
        }
        if (status === ScrapperStatus.unknown) {
            try {
                await this.driver.findElement(By.id('Free-5X-Search'))
                status = ScrapperStatus.captchaLevel2
            } catch (e) {
            }
        }
        if (status === ScrapperStatus.unknown) {
            try {
                await this.driver.findElement(By.id('reload-button'))
                status = ScrapperStatus.loadingError
            } catch (e) {
            }
        }
        if (status === ScrapperStatus.unknown) {
            status = ScrapperStatus.ok
        }
        this.emit('status', status)
        return status
    }

    protected getQuery(referenceNumber: number | string) {
        return `https://www.equineline.com/Free-5X-Pedigree.cfm?source=RISA&page_state=GENERATE&nicking_stats_indicator=Y&reference_number=${referenceNumber}`
    }

    public initializeServer() {
        return new Promise(resolve => {
            this.baseServer = createServer((req, res) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'text/html');
                res.end(`<h1>Equineline Pedigree scrapper</h1> <p>downloaded ${this.logReferenceNumber}</p>`);
            }).listen(this.baseServerPort, resolve)
        })
    }

    public async initializeDriver(browser: 'chrome' | 'firefox') {
        if (browser === 'firefox') {
            this.driver = this.createFireFox()
        } else {
            this.driver = this.createChrome()
        }
    }

    public downloadVerified() {
        if (this.timeout) {
            clearTimeout(this.timeout)
            con.log('download Verified', this.logReferenceNumber)
        }
        this.timeout = false
    }

    public async download(referenceNumber: number | string) {
        if (this.timeout !== false) {
            if (this.logReferenceNumber !== referenceNumber) {
                throw new Error('wrong download')
            }
        } else {
            this.timeout = setTimeout(() => {
                this.timeout = false
                this.reloadPage(referenceNumber)
            }, 5 * 60 * 1000)
        }
        if (this.failedTryCount.ref === referenceNumber.toString()) {
            this.failedTryCount.count++
            if (this.failedTryCount.count === 6) {
                this.failedTryCount.count = 0
                con.log('failed 5 times', referenceNumber)
                con.log('reset downloader', referenceNumber)
                return this.captchaLevelTwoHandler(referenceNumber)
            }
        } else {
            this.failedTryCount.ref = referenceNumber.toString()
            this.failedTryCount.count = 0
        }
        this.logReferenceNumber = referenceNumber.toString()
        await this.driver.navigate().to(`http://127.0.0.1:${this.baseServerPort}`)
        await sleep(100);
        await this.driver.navigate().to(this.getQuery(referenceNumber))
        const status = await this.checkStatus();
        con.log("status is", ScrapperStatus[status])
        if (status === ScrapperStatus.captchaLevel1) {
            await this.captchaLevelOneHandler()
        } else if (status === ScrapperStatus.captchaLevel2) {
            await this.captchaLevelTwoHandler(referenceNumber)
        } else if (status === ScrapperStatus.loadingError) {
            await this.reloadPage(referenceNumber)
        } else if (status === ScrapperStatus.unknown) {
            throw new Error('unexpected situation (scrapper cant detect situation "captchaLevel1 or captchaLevel2 or ok")')
        } else {
            this.emit('downloadCompleted', referenceNumber)
        }
    }

    public async close() {
        if (this.baseServer) {
            this.baseServer.close()
        }
        if (this.driver) {
            await this.driver.close()
        }
    }
}