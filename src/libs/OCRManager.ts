import * as fs from 'fs'
import {PNG} from 'pngjs'
import {join} from 'path'
import {deleteFolderRecursive, dropSimilar, sleep} from "./Utils"
import {Horse, NickingStats, Pedigree} from './DatabaseManager'
import {exec} from "child_process"
import ProgressBar from 'progress'


export class OCRManager {
    protected OCRTempPath: string;
    protected forceToUpdate: boolean

    constructor(OCRTempPath: string, forceToUpdate: boolean = false) {
        this.OCRTempPath = OCRTempPath
        this.forceToUpdate = forceToUpdate
    }

    protected convertPDFToImage(address: string, id: string): Promise<boolean | string> {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                if (process.platform === 'win32') {
                    const command = exec(`gm.exe convert  -density 300  -size 1275x1651 ${address} -resize 1275x1651  ${join(this.OCRTempPath, id + "-1.png")}`)
                    command.on('error', reject)
                    command.on('exit', code => {
                        const command2 = exec(`gm.exe convert  -density 300  -size 1275x1651 ${address}[1] -resize 1275x1651  ${join(this.OCRTempPath, id + "-2.png")}`)
                        command2.on('error', reject)
                        command2.on('exit', code => resolve(code === 0 ? join(this.OCRTempPath, id) : false))
                    })
                } else {
                    const command = exec(`pdftoppm ${address} ${join(this.OCRTempPath, id)} -png`)
                    command.on('error', reject)
                    command.on('exit', code => {
                        if (code === 0) resolve(join(this.OCRTempPath, id))
                        else reject()
                    })
                }
            }, 100)
        })
    }

    public readImage(address: string, id: string, deleteTmpImage: boolean = true): Promise<{ horse: Horse, pedigree: Pedigree, nickingStats: NickingStats }> {
        const progress = new ProgressBar('  OCR [:bar] :percent', {
            complete: '=',
            incomplete: ' ',
            width: 20,
            total: 72 + 43
        });
        let finalResult: { horse: Horse, pedigree: Pedigree, nickingStats: NickingStats } = {
            horse: new Horse,
            pedigree: new Pedigree,
            nickingStats: new NickingStats
        }
        let path: string | boolean
        return new Promise(async (resolve, reject) => {
            let error;
            for (let i = 0; i < 5; i++) {
                try {
                    path = await this.convertPDFToImage(address, id)
                    console.log(path)
                    break
                } catch (e) {
                    error = e
                    await sleep(500)
                }
            }
            if (error) reject(error)
            progress.tick()
            if (typeof path === 'string') {
                const self = this
                fs.createReadStream(path + "-1.png")
                    .pipe(
                        new PNG({
                            filterType: 4,
                        })
                    )
                    .on("parsed", async function () {
                        const {xArray, yArray} = await self.findLines(this)
                        progress.tick()
                        await self.cropImages(this, id, xArray, yArray, progress)
                        const result = await self.OCR(id, progress)
                        progress.tick()
                        finalResult.horse = result.horse
                        finalResult.pedigree = result.pedigree
                        fs.createReadStream(path + "-2.png")
                            .pipe(
                                new PNG({
                                    filterType: 4,
                                })
                            )
                            .on("parsed", async function () {
                                const yArray = [
                                    245,

                                    315,

                                    390,

                                ]
                                const xArray = [
                                    150,
                                    300,
                                    450,
                                    600,
                                    750,
                                    900,
                                    1050,
                                    1200
                                ]
                                progress.tick() // 1
                                const images = await self.cropImagesPage2(this, xArray, yArray, id, progress)
                                const result = await self.OCRPage2(id, images, progress)
                                finalResult.nickingStats = result
                                resolve(finalResult)
                                if (deleteTmpImage) {
                                    deleteFolderRecursive(join(self.OCRTempPath, id + "-tmp"));
                                    deleteFolderRecursive(join(self.OCRTempPath, id + "-tmp-2"));
                                    await fs.promises.unlink(join(self.OCRTempPath, id + "-1.png"))
                                    await fs.promises.unlink(join(self.OCRTempPath, id + "-2.png"))
                                }
                            })
                    });
            }
        })
    }

    protected async findLines(image: PNG) {
        const threshold = 120
        const defaultXPadding = 36
        let targets = []
        let xMap: { [key: string]: number } = {}
        // extract y lines
        for (let y = 0; y < image.height; y++) {
            let blackPixelCount = 0;
            for (let x = 0; x < image.width; x++) {
                let idx = (image.width * y + x) << 2;
                const r = image.data[idx]
                const g = image.data[idx + 1]
                const b = image.data[idx + 2]
                const a = image.data[idx + 3]
                if (r === g && g === b && (b === a && a === b || b === 230 || b === 235)) {
                    image.data[idx + 3] = 0;
                } else if (r === g && g === b) {
                    if (r < threshold) {
                        image.data[idx] = 0
                        image.data[idx + 1] = 0
                        image.data[idx + 2] = 0
                    }
                }
                if (!xMap.hasOwnProperty(x.toString())) {
                    xMap[x.toString()] = 0;
                }
                if (r === g && g === b && b === 0 && a !== 0) {
                    blackPixelCount++;
                    xMap[x.toString()] += 1
                }
            }
            if (blackPixelCount >= image.width - 2 * defaultXPadding) {
                targets.push(y)
            }
            blackPixelCount = 0;
        }
        let xArray = []
        for (let key in xMap) {
            xArray.push({xIndex: Number(key), count: xMap[key]})
        }
        targets = dropSimilar(targets)
        const targetImageHeight = targets[targets.length - 1] - targets[0] - 3
        const targetImageWidth = image.width - 2 * defaultXPadding - 6
        xArray = xArray
            .filter(a => a.count >= targetImageHeight && (defaultXPadding + 3 < a.xIndex && a.xIndex < image.width - defaultXPadding - 3))
            .sort((a, b) => a.xIndex - b.xIndex)
            .filter((a, index) => index % 2 === 1)
            .map(r => r.xIndex)
        if (xArray.length < 5) {
            await fs.promises.writeFile(join(this.OCRTempPath, "debug.png"), PNG.sync.write(image))
            throw new Error('cant find lines')
        }
        xArray.push(image.width - defaultXPadding - 2)
        xArray.unshift(defaultXPadding - 2)
        // extract x lines
        const yArray = [];
        for (let i = 0; i < 6; i++) {
            const boxCount = Math.pow(2, i)
            const lineCount = boxCount - 1;
            let miniTarget = []
            for (let y = targets[0] + 2; y < targets[targets.length - 1] - 2; y++) {
                let blackPixelCount = 0;
                for (let x = xArray[i]; x < xArray[i + 1]; x++) {
                    let idx = (image.width * y + x) << 2;
                    const r = image.data[idx];
                    const g = image.data[idx + 1];
                    const b = image.data[idx + 2];
                    const a = image.data[idx + 3];
                    if (r === g && g === b && b === 0 && a !== 0) {
                        blackPixelCount++;
                    }
                }
                if (blackPixelCount >= xArray[i + 1] - xArray[i]) {
                    miniTarget.push(y)
                }
            }
            miniTarget = dropSimilar(miniTarget)
            yArray[i] = miniTarget
            yArray[i].unshift(targets[0] + 2)
            yArray[i].push(targets[targets.length - 1] - 2)
        }
        return {xArray, yArray}
    }

    protected async cropImages(image: PNG, id: string, xArray: Array<number>, yArray: Array<Array<number>>, progress: ProgressBar) {
        // crop image
        try {
            await fs.promises.mkdir(join(this.OCRTempPath, id + "-tmp"));
        } catch (e) {
            if (e.errno !== -17) {
                throw e;
            }
        }
        for (let i = 0; i < 6; i++) {
            const boxCount = Math.pow(2, i)
            for (let j = 0; j < boxCount; j++) {
                const pos = {
                    x: xArray[i],
                    y: yArray[i][j],
                    w: xArray[i + 1] - xArray[i],
                    h: yArray[i][j + 1] - yArray[i][j]
                }
                const dst = new PNG({width: pos.w, height: pos.h})
                image.bitblt(dst, pos.x, pos.y, pos.w, pos.h, 0, 0)
                await fs.promises.writeFile(join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png"), PNG.sync.write(dst))
            }
            progress.tick()
        }
    }

    protected async cropImagesPage2(image: PNG, xArray: Array<number>, yArray: Array<number>, id: string, progress: ProgressBar): Promise<Array<Array<string>>> {
        try {
            await fs.promises.mkdir(join(this.OCRTempPath, id + "-tmp-2"));
        } catch (e) {
            if (e.errno !== -17) {
                throw e;
            }
        }
        const results: Array<Array<string>> = []
        for (let j = 0; j < yArray.length; j++) {
            results[j] = []
            for (let i = 0; i < xArray.length - 1; i++) {
                const pos = {
                    x: xArray[i],
                    y: yArray[j],
                    w: xArray[i + 1] - xArray[i],
                    h: 30
                }
                const dst = new PNG({width: pos.w, height: pos.h})
                image.bitblt(dst, pos.x, pos.y, pos.w, pos.h, 0, 0)
                await fs.promises.writeFile(join(this.OCRTempPath, id + "-tmp-2", j + "-" + i + ".png"), PNG.sync.write(dst))
                results[j].push(join(this.OCRTempPath, id + "-tmp-2", j + "-" + i + ".png"))
                progress.tick() // 21
            }
        }
        return results
    }

    protected async OCR(id: string, progress: ProgressBar): Promise<{ horse: Horse, pedigree: Pedigree }> {
        let p: Pedigree
        let h: Horse
        if (this.forceToUpdate) {
            let tp = await Pedigree.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            })
            let th = await Horse.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            })
            p = (!tp) ? new Pedigree() : tp;
            h = (!th) ? new Horse() : th;
        } else {
            p = new Pedigree()
            h = new Horse()
        }
        const horseText = await this.doOCR(join(this.OCRTempPath, id + "-tmp", 0 + "-" + 0 + ".png")).then(r => r.trim().split(/\r?\n|\r/g).filter(r => r.length > 0))
        h.referenceNumber = Number(id)
        h.name = horseText[0] || ''
        if (horseText[2].toLowerCase().startsWith('foaled')) {
            h.type = horseText[1] || ''
            h.foaledDate = horseText[2] || ''
            h.foaledIn = horseText[3] || ''
            h.startedGame = ((horseText[4] || '').endsWith('Starts')) ? Number(horseText[4].split(' ')[0]) : NaN
            h.winner = ((horseText[4] || '').endsWith('Winner')) ? Number(horseText[4].split(' ')[0]) : NaN
        } else {
            h.type = horseText[1] + " " + horseText[2] || ''
            h.foaledDate = horseText[3] || ''
            h.foaledIn = horseText[4] || ''
            h.startedGame = ((horseText[5] || '').endsWith('Starts')) ? Number(horseText[5].split(' ')[0]) : NaN
            h.winner = ((horseText[5] || '').endsWith('Winner')) ? Number(horseText[5].split(' ')[0]) : NaN
        }
        h.rawString = horseText[4] || '' + horseText[5] || '';
        progress.tick()
        const texts: Array<Array<string>> = []
        for (let i = 1; i < 6; i++) {
            const boxCount = Math.pow(2, i)
            texts[i] = []
            for (let j = 0; j < boxCount; j++) {
                texts[i][j] = await this.doOCR(join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png")).then(r => r.replace(/\r?\n|\r/g, '').trim())
                progress.tick()
            }
        }
        p.referenceNumber = h.referenceNumber
        p.S = texts[1][0] || ""
        p.D = texts[1][1] || ""
        p.SS = texts[2][0] || ""
        p.SD = texts[2][1] || ""
        p.DS = texts[2][2] || ""
        p.DD = texts[2][3] || ""
        p.SSS = texts[3][0] || ""
        p.SSD = texts[3][1] || ""
        p.SDS = texts[3][2] || ""
        p.SDD = texts[3][3] || ""
        p.DSS = texts[3][4] || ""
        p.DSD = texts[3][5] || ""
        p.DDS = texts[3][6] || ""
        p.DDD = texts[3][7] || ""
        p.SSSS = texts[4][0] || ""
        p.SSSD = texts[4][1] || ""
        p.SSDS = texts[4][2] || ""
        p.SSDD = texts[4][3] || ""
        p.SDSS = texts[4][4] || ""
        p.SDSD = texts[4][5] || ""
        p.SDDS = texts[4][6] || ""
        p.SDDD = texts[4][7] || ""
        p.DSSS = texts[4][8] || ""
        p.DSSD = texts[4][9] || ""
        p.DSDS = texts[4][10] || ""
        p.DSDD = texts[4][11] || ""
        p.DDSS = texts[4][12] || ""
        p.DDSD = texts[4][13] || ""
        p.DDDS = texts[4][14] || ""
        p.DDDD = texts[4][15] || ""
        p.SSSSS = texts[5][0] || ""
        p.SSSSD = texts[5][1] || ""
        p.SSSDS = texts[5][2] || ""
        p.SSSDD = texts[5][3] || ""
        p.SSDSS = texts[5][4] || ""
        p.SSDSD = texts[5][5] || ""
        p.SSDDS = texts[5][6] || ""
        p.SSDDD = texts[5][7] || ""
        p.SDSSS = texts[5][8] || ""
        p.SDSSD = texts[5][9] || ""
        p.SDSDS = texts[5][10] || ""
        p.SDSDD = texts[5][11] || ""
        p.SDDSS = texts[5][12] || ""
        p.SDDSD = texts[5][13] || ""
        p.SDDDS = texts[5][14] || ""
        p.SDDDD = texts[5][15] || ""
        p.DSSSS = texts[5][16] || ""
        p.DSSSD = texts[5][17] || ""
        p.DSSDS = texts[5][18] || ""
        p.DSSDD = texts[5][19] || ""
        p.DSDSS = texts[5][20] || ""
        p.DSDSD = texts[5][21] || ""
        p.DSDDS = texts[5][22] || ""
        p.DSDDD = texts[5][23] || ""
        p.DDSSS = texts[5][24] || ""
        p.DDSSD = texts[5][25] || ""
        p.DDSDS = texts[5][26] || ""
        p.DDSDD = texts[5][27] || ""
        p.DDDSS = texts[5][28] || ""
        p.DDDSD = texts[5][29] || ""
        p.DDDDS = texts[5][30] || ""
        p.DDDDD = texts[5][31] || ""
        return {horse: h, pedigree: p}
    }

    protected async OCRPage2(id: string, images: Array<Array<string>>, progress: ProgressBar): Promise<NickingStats> {
        let t
        if (this.forceToUpdate) {
            let tmp = await NickingStats.findOne({
                where: {
                    referenceNumber: Number(id)
                }
            })
            t = (!tmp) ? new NickingStats() : tmp;
        } else {
            t = new NickingStats()
        }
        const texts: Array<Array<string>> = []
        for (let j = 0; j < 3; j++) {
            texts[j] = []
            for (let i = 0; i < 7; i++) {
                if (images[j][i]) {
                    texts[j][i] = await this.doOCR(images[j][i]).then(s => s.trim())
                    progress.tick()
                } else {
                    console.log(j, i)
                }
            }
        }
        t.referenceNumber = Number(id)
        t.s_foals = texts[0][1] || ''
        t.s_starters = texts[0][2] || ''
        t.s_winners = texts[0][3] || ''
        t.s_bw = texts[0][4] || ''
        t.s_earnings = texts[0][5] || ''
        t.s_aei = texts[0][6] || ''
        t.bs_mares = texts[1][0] || ''
        t.bs_foals = texts[1][1] || ''
        t.bs_starters = texts[1][2] || ''
        t.bs_winners = texts[1][3] || ''
        t.bs_bw = texts[1][4] || ''
        t.bs_earnings = texts[1][5] || ''
        t.bs_aei = texts[1][6] || ''
        t.bs_s_mares = texts[2][0] || ''
        t.bs_s_foals = texts[2][1] || ''
        t.bs_s_starters = texts[2][2] || ''
        t.bs_s_winners = texts[2][3] || ''
        t.bs_s_bw = texts[2][4] || ''
        t.bs_s_earnings = texts[2][5] || ''
        t.bs_s_aei = texts[2][6] || ''
        return t;
    }

    protected PPOCR(id: string, progress: ProgressBar): Promise<{ horse: Horse, pedigree: Pedigree }> {
        return new Promise(async resolve => {
            let p: Pedigree
            let h: Horse
            if (this.forceToUpdate) {
                let tp = await Pedigree.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                })
                let th = await Horse.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                })
                p = (!tp) ? new Pedigree() : tp;
                h = (!th) ? new Horse() : th;
            } else {
                p = new Pedigree()
                h = new Horse()
            }
            const horseText = await this.doOCR(join(this.OCRTempPath, id + "-tmp", 0 + "-" + 0 + ".png")).then(r => r.trim().split(/\r?\n|\r/g).filter(r => r.length > 0))
            h.referenceNumber = Number(id)
            h.name = horseText[0] || ''
            if (horseText[2].toLowerCase().startsWith('foaled')) {
                h.type = horseText[1] || ''
                h.foaledDate = horseText[2] || ''
                h.foaledIn = horseText[3] || ''
                h.startedGame = ((horseText[4] || '').endsWith('Starts')) ? Number(horseText[4].split(' ')[0]) : NaN
                h.winner = ((horseText[4] || '').endsWith('Winner')) ? Number(horseText[4].split(' ')[0]) : NaN
            } else {
                h.type = horseText[1] + " " + horseText[2] || ''
                h.foaledDate = horseText[3] || ''
                h.foaledIn = horseText[4] || ''
                h.startedGame = ((horseText[5] || '').endsWith('Starts')) ? Number(horseText[5].split(' ')[0]) : NaN
                h.winner = ((horseText[5] || '').endsWith('Winner')) ? Number(horseText[5].split(' ')[0]) : NaN
            }
            h.rawString = horseText[4] || '' + horseText[5] || '';
            progress.tick()
            const texts: Array<Array<string>> = []
            let done = 0;
            const max = 62;

            function cb(text: string, i: number, j: number) {
                console.log('1', i, j, 'done')
                texts[i][j] = text
                progress.tick()
                done++
                if (done === max) {
                    p.referenceNumber = h.referenceNumber
                    p.S = texts[1][0] || ""
                    p.D = texts[1][1] || ""
                    p.SS = texts[2][0] || ""
                    p.SD = texts[2][1] || ""
                    p.DS = texts[2][2] || ""
                    p.DD = texts[2][3] || ""
                    p.SSS = texts[3][0] || ""
                    p.SSD = texts[3][1] || ""
                    p.SDS = texts[3][2] || ""
                    p.SDD = texts[3][3] || ""
                    p.DSS = texts[3][4] || ""
                    p.DSD = texts[3][5] || ""
                    p.DDS = texts[3][6] || ""
                    p.DDD = texts[3][7] || ""
                    p.SSSS = texts[4][0] || ""
                    p.SSSD = texts[4][1] || ""
                    p.SSDS = texts[4][2] || ""
                    p.SSDD = texts[4][3] || ""
                    p.SDSS = texts[4][4] || ""
                    p.SDSD = texts[4][5] || ""
                    p.SDDS = texts[4][6] || ""
                    p.SDDD = texts[4][7] || ""
                    p.DSSS = texts[4][8] || ""
                    p.DSSD = texts[4][9] || ""
                    p.DSDS = texts[4][10] || ""
                    p.DSDD = texts[4][11] || ""
                    p.DDSS = texts[4][12] || ""
                    p.DDSD = texts[4][13] || ""
                    p.DDDS = texts[4][14] || ""
                    p.DDDD = texts[4][15] || ""
                    p.SSSSS = texts[5][0] || ""
                    p.SSSSD = texts[5][1] || ""
                    p.SSSDS = texts[5][2] || ""
                    p.SSSDD = texts[5][3] || ""
                    p.SSDSS = texts[5][4] || ""
                    p.SSDSD = texts[5][5] || ""
                    p.SSDDS = texts[5][6] || ""
                    p.SSDDD = texts[5][7] || ""
                    p.SDSSS = texts[5][8] || ""
                    p.SDSSD = texts[5][9] || ""
                    p.SDSDS = texts[5][10] || ""
                    p.SDSDD = texts[5][11] || ""
                    p.SDDSS = texts[5][12] || ""
                    p.SDDSD = texts[5][13] || ""
                    p.SDDDS = texts[5][14] || ""
                    p.SDDDD = texts[5][15] || ""
                    p.DSSSS = texts[5][16] || ""
                    p.DSSSD = texts[5][17] || ""
                    p.DSSDS = texts[5][18] || ""
                    p.DSSDD = texts[5][19] || ""
                    p.DSDSS = texts[5][20] || ""
                    p.DSDSD = texts[5][21] || ""
                    p.DSDDS = texts[5][22] || ""
                    p.DSDDD = texts[5][23] || ""
                    p.DDSSS = texts[5][24] || ""
                    p.DDSSD = texts[5][25] || ""
                    p.DDSDS = texts[5][26] || ""
                    p.DDSDD = texts[5][27] || ""
                    p.DDDSS = texts[5][28] || ""
                    p.DDDSD = texts[5][29] || ""
                    p.DDDDS = texts[5][30] || ""
                    p.DDDDD = texts[5][31] || ""
                    resolve({horse: h, pedigree: p})
                }
            }

            for (let i = 1; i < 6; i++) {
                const boxCount = Math.pow(2, i)
                texts[i] = []
                for (let j = 0; j < boxCount; j++) {
                    console.log('1', i, j, 'start')
                    this.doOCR(join(this.OCRTempPath, id + "-tmp", i + "-" + j + ".png"))
                        .then(r => r.replace(/\r?\n|\r/g, '').trim())
                        .then(r => cb(r, i, j))
                }
            }
        })
    }

    protected async PPOCRPage2(id: string, images: Array<Array<string>>, progress: ProgressBar): Promise<NickingStats> {
        return new Promise(async resolve => {
            let t: NickingStats
            if (this.forceToUpdate) {
                let tmp = await NickingStats.findOne({
                    where: {
                        referenceNumber: Number(id)
                    }
                })
                t = (!tmp) ? new NickingStats() : tmp;
            } else {
                t = new NickingStats()
            }

            const texts: Array<Array<string>> = []
            const max = 21;
            let done = 0;

            function cb(text: string, i: number, j: number) {
                console.log('2', i, j, 'done')
                texts[j][i] = text
                progress.tick()
                done++
                if (done === max) {
                    t.referenceNumber = Number(id)
                    t.s_foals = texts[0][1] || ''
                    t.s_starters = texts[0][2] || ''
                    t.s_winners = texts[0][3] || ''
                    t.s_bw = texts[0][4] || ''
                    t.s_earnings = texts[0][5] || ''
                    t.s_aei = texts[0][6] || ''
                    t.bs_mares = texts[1][0] || ''
                    t.bs_foals = texts[1][1] || ''
                    t.bs_starters = texts[1][2] || ''
                    t.bs_winners = texts[1][3] || ''
                    t.bs_bw = texts[1][4] || ''
                    t.bs_earnings = texts[1][5] || ''
                    t.bs_aei = texts[1][6] || ''
                    t.bs_s_mares = texts[2][0] || ''
                    t.bs_s_foals = texts[2][1] || ''
                    t.bs_s_starters = texts[2][2] || ''
                    t.bs_s_winners = texts[2][3] || ''
                    t.bs_s_bw = texts[2][4] || ''
                    t.bs_s_earnings = texts[2][5] || ''
                    t.bs_s_aei = texts[2][6] || ''
                    resolve(t)
                }
            }

            for (let j = 0; j < 3; j++) {
                texts[j] = []
                for (let i = 0; i < 7; i++) {
                    if (images[j][i]) {
                        console.log('2', i, j, 'start')
                        this.doOCR(images[j][i]).then(s => s.trim()).then(r => cb(r, i, j))
                    } else {
                        console.log(j, i)
                    }
                }
            }
        })
    }

    protected doOCR(address: string): Promise<string> {
        return new Promise(async (resolve, reject) => {
            if (process.platform === 'win32') {
                const command = exec(`tesseract.exe ${address} stdout`, (error, stdout) => {
                    if (error) reject(error)
                    else {
                        resolve(stdout)
                    }
                })
            } else {
                const command = exec(`tesseract ${address} stdout`, (error, stdout) => {
                    if (error) reject(error)
                    else {
                        resolve(stdout)
                    }
                })
            }
        })
    }
}